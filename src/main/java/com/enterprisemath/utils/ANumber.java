package com.enterprisemath.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import org.apache.commons.lang3.StringUtils;

/**
 * Object for arbitrary precision numbers.
 * This is used for exact, but simple money calculations.
 * In this class precision has same meaning as in the standard java BigDecimal class.
 * That means number of digits representing the number counted from to most left non zero one.
 * Rounding mode is always DOWN (i.e. truncation) for all operations, unless explicitly specified.
 *
 * @author radek.hecl
 */
public final class ANumber implements Comparable<ANumber> {

    /**
     * Zero.
     */
    public static ANumber ZERO = new ANumber("0");

    /**
     * One.
     */
    public static ANumber ONE = new ANumber("1");

    /**
     * Ten.
     */
    public static ANumber TEN = new ANumber("10");

    /**
     * Precise number representation.
     */
    private BigDecimal number;

    /**
     * Prevents construction from outside.
     */
    private ANumber() {
    }

    /**
     * Creates new instance.
     *
     * @param number number
     */
    private ANumber(BigDecimal number) {
        this.number = number.stripTrailingZeros();
    }

    /**
     * Creates new instance.
     *
     * @param s string representation
     */
    private ANumber(String s) {
        this.number = new BigDecimal(s).stripTrailingZeros();
    }

    /**
     * Performs addition operation.
     *
     * @param x other number
     * @return addition operation result
     */
    public ANumber add(ANumber x) {
        return new ANumber(number.add(x.number));
    }

    /**
     * Performs addition operation.
     *
     * @param x other number
     * @return addition operation result
     */
    public ANumber add(double x) {
        return add(new ANumber(String.valueOf(x)));
    }

    /**
     * Performs subtraction operation.
     *
     * @param x other number
     * @return addition subtraction result
     */
    public ANumber sub(ANumber x) {
        return new ANumber(number.subtract(x.number));
    }

    /**
     * Performs subtraction operation.
     *
     * @param x other number
     * @return addition subtraction result
     */
    public ANumber sub(double x) {
        return sub(new ANumber(String.valueOf(x)));
    }

    /**
     * Performs multiplication operation.
     *
     * @param x other number
     * @return addition multiplication result
     */
    public ANumber mul(ANumber x) {
        return new ANumber(number.multiply(x.number));
    }

    /**
     * Performs multiplication operation.
     *
     * @param x other number
     * @return addition multiplication result
     */
    public ANumber mul(double x) {
        return mul(new ANumber(String.valueOf(x)));
    }

    /**
     * Multiplies this number by 10^n.
     *
     * @param n factor
     * @return multiplied number
     */
    public ANumber mul10Pow(int n) {
        return mul(ANumber.create("1e" + n));
    }

    /**
     * Performs division operation.
     *
     * @param x divisor number
     * @param precision precision of the result (see the class level comment for details)
     * @return division operation result
     */
    public ANumber div(ANumber x, int precision) {
        return new ANumber(number.divide(x.number, new MathContext(precision, RoundingMode.DOWN)));
    }

    /**
     * Performs division operation.
     *
     * @param x divisor number
     * @param precision precision of the result (see the class level comment for details)
     * @return division operation result
     */
    public ANumber div(double x, int precision) {
        return div(new ANumber(String.valueOf(x)), precision);
    }

    /**
     * Performs division up to the integer part. All the decimals are truncated.
     *
     * @param x divisor number
     * @return division operation result
     */
    public ANumber divToIntegralValue(ANumber x) {
        return ANumber.create(number.divideToIntegralValue(x.toBigDecimal()));
    }

    /**
     * Performs division up to the integer part. All the decimals are truncated.
     *
     * @param x divisor number
     * @return division operation result
     */
    public ANumber divToIntegralValue(double x) {
        return divToIntegralValue(new ANumber(String.valueOf(x)));
    }

    /**
     * Performs division operation and returns the result with remainder.
     *
     * @param x divisor number
     * @param precision precision of the result (see the class level comment for details)
     * @return division operation result
     */
    public ANumberRemainderPair divWithRemainder(ANumber x, int precision) {
        BigDecimal div = number.divide(x.number, new MathContext(precision, RoundingMode.DOWN));
        BigDecimal rem = number.subtract(div.multiply(x.number));
        return ANumberRemainderPair.create(new ANumber(div), new ANumber(rem));
    }

    /**
     * Performs division operation and returns the result with remainder.
     *
     * @param x divisor number
     * @param precision precision of the result (see the class level comment for details)
     * @return division operation result
     */
    public ANumberRemainderPair divWithRemainder(double x, int precision) {
        return divWithRemainder(new ANumber(String.valueOf(x)), precision);
    }

    /**
     * Negates this number.
     *
     * @return negative of this number
     */
    public ANumber negate() {
        return ANumber.create(number.negate());
    }

    /**
     * Returns absolute value of this number.
     *
     * @return absolute value of this number
     */
    public ANumber abs() {
        return ANumber.create(number.abs());
    }

    /**
     * Returns this number powered by n.
     *
     * @param n power
     * @param precision precision of the result (see the class level comment for details)
     * @return power operation result
     */
    public ANumber pow(int n, int precision) {
        return ANumber.create(number.pow(n, new MathContext(precision, RoundingMode.DOWN)));
    }

    /**
     * Truncates all the fractional part of this number.
     * This is similar to narrowing type from double to long.
     *
     * @return number without part right from the decimal point
     */
    public ANumber truncDecimals() {
        return new ANumber(new BigDecimal(number.toBigInteger()));
    }

    /**
     * Truncates all fractional part of this number after the specified number maximum decimals.
     * This is equal to multiplying the number by 10^maxDecimals, truncating all decimals and dividing it back by the same number.
     *
     * @param maxDecimals maximum number of allowed decimals after truncation
     * @return number truncated up to the maximum allowed number of decimals
     */
    public ANumber truncDecimals(int maxDecimals) {
        ANumber fact = ANumber.create("1e" + maxDecimals);
        ANumber invfact = ANumber.create("1e-" + maxDecimals);
        return mul(fact).truncDecimals().mul(invfact);
    }

    /**
     * Returns whether this number is equal with the other number.
     *
     * @param x tested number
     * @return true if this number is equal to the other number
     */
    public boolean eq(ANumber x) {
        return number.compareTo(x.number) == 0;
    }

    /**
     * Returns whether this number is equal with the other number.
     *
     * @param x tested number
     * @return true if this number is equal to the other number
     */
    public boolean eq(double x) {
        return eq(new ANumber(String.valueOf(x)));
    }

    /**
     * Returns whether this number is not equal with the other number.
     *
     * @param x tested number
     * @return true if this number is not equal to the other number
     */
    public boolean neq(ANumber x) {
        return number.compareTo(x.number) != 0;
    }

    /**
     * Returns whether this number is not equal with the other number.
     *
     * @param x tested number
     * @return true if this number is not equal to the other number
     */
    public boolean neq(double x) {
        return neq(new ANumber(String.valueOf(x)));
    }

    /**
     * Returns whether this number is greater than the other number.
     *
     * @param x tested number
     * @return true if this number is greater than the other one
     */
    public boolean g(ANumber x) {
        return number.compareTo(x.number) == 1;
    }

    /**
     * Returns whether this number is greater than the other number.
     *
     * @param x tested number
     * @return true if this number is greater than the other one
     */
    public boolean g(double x) {
        return g(new ANumber(String.valueOf(x)));
    }

    /**
     * Returns whether this number is greater or equal to the other number.
     *
     * @param x tested number
     * @return true if this number is greater or equal to the other one
     */
    public boolean geq(ANumber x) {
        return number.compareTo(x.number) >= 0;
    }

    /**
     * Returns whether this number is greater or equal to the other number.
     *
     * @param x tested number
     * @return true if this number is greater or equal to the other one
     */
    public boolean geq(double x) {
        return geq(ANumber.create(String.valueOf(x)));
    }

    /**
     * Returns whether this number is less than the other number.
     *
     * @param x tested number
     * @return true if this number is less than the other one
     */
    public boolean l(ANumber x) {
        return number.compareTo(x.number) < 0;
    }

    /**
     * Returns whether this number is less than the other number.
     *
     * @param x tested number
     * @return true if this number is less than the other one
     */
    public boolean l(double x) {
        return l(ANumber.create(String.valueOf(x)));
    }

    /**
     * Returns whether this number is less or equal to the other number.
     *
     * @param x tested number
     * @return true if this number is less than the other one
     */
    public boolean leq(ANumber x) {
        return number.compareTo(x.number) <= 0;
    }

    /**
     * Returns whether this number is less or equal to the other number.
     *
     * @param x tested number
     * @return true if this number is less than the other one
     */
    public boolean leq(double x) {
        return leq(ANumber.create(String.valueOf(x)));
    }

    /**
     * Returns this number as a big decimal.
     *
     * @return this number as a big decimal
     */
    public BigDecimal toBigDecimal() {
        return number;
    }

    /**
     * Converts this number to the full, containing everything that can be restored.
     *
     * @return number in it's string representation
     */
    public String toFullString() {
        return number.toPlainString();
    }

    /**
     * Converts number to the string with fixed decimal digits.
     * This method might truncate some numbers or add extra zeros at the end.
     *
     * @param numDecimals number of decimals, must be non negative
     * @return number as a plain string with specified number of decimals
     */
    public String toFixedDecimalString(int numDecimals) {
        Guard.notNegative(numDecimals, "numDecimals must be >= 0");
        String str = number.toPlainString();
        String[] parts = str.split("\\.");
        if (parts.length == 1) {
            if (numDecimals == 0) {
                return parts[0];
            }
            else {
                return parts[0] + "." + StringUtils.repeat("0", numDecimals);
            }
        }
        else if (parts.length == 2) {
            if (numDecimals == 0) {
                return parts[0];
            }
            else if (parts[1].length() > numDecimals) {
                return parts[0] + "." + parts[1].substring(0, numDecimals);
            }
            else if (parts[1].length() < numDecimals) {
                return str + StringUtils.repeat("0", numDecimals - parts[1].length());
            }
            else {
                return str;
            }
        }
        else {
            throw new RuntimeException("unexpected number of '.': " + str);
        }
    }

    /**
     * Returns double representation of this number.
     * Conversion might ended up by loosing the precision.
     *
     * @return double representation of this value
     */
    public double doubleValue() {
        return number.doubleValue();
    }

    @Override
    public int compareTo(ANumber x) {
        return number.compareTo(x.number);
    }

    @Override
    public int hashCode() {
        return Dut.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return Dut.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return "ANumber[" + number.toString() + "]";
    }

    /**
     * Creates new instance. This method is capable to parse any result of toFullString method.
     *
     * @param s source string
     * @return created number
     */
    public static ANumber create(String s) {
        return new ANumber(s);
    }

    /**
     * Creates new instance.
     *
     * @param number number
     * @return created number
     */
    public static ANumber create(double number) {
        return new ANumber(new BigDecimal(String.valueOf(number)));
    }

    /**
     * Creates new instance.
     *
     * @param number number
     * @return created number
     */
    public static ANumber create(BigInteger number) {
        return new ANumber(new BigDecimal(number));
    }

    /**
     * Creates new instance.
     *
     * @param number number
     * @return created number
     */
    public static ANumber create(BigDecimal number) {
        return new ANumber(number);
    }

    /**
     * Returns the minimum number from the lists of numbers.
     *
     * @param first first number
     * @param others other numbers
     * @return minimum number
     */
    public static ANumber min(ANumber first, ANumber... others) {
        ANumber res = first;
        for (int i = 0; i < others.length; ++i) {
            if (others[i].l(res)) {
                res = others[i];
            }
        }
        return res;
    }

    /**
     * Returns the maximum number from the lists of numbers.
     *
     * @param first first number
     * @param others other numbers
     * @return maximum number
     */
    public static ANumber max(ANumber first, ANumber... others) {
        ANumber res = first;
        for (int i = 0; i < others.length; ++i) {
            if (others[i].g(res)) {
                res = others[i];
            }
        }
        return res;
    }

}
