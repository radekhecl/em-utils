package com.enterprisemath.utils;

import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * Test case which proves the id generator.
 *
 * @author radek.hecl
 */
public class SequenceUuidGeneratorTest {

    /**
     * Creates new instance.
     */
    public SequenceUuidGeneratorTest() {
    }

    /**
     * Tests id generation.
     */
    @Test
    public void testGenerateId() {
        IdGenerator generator = null;

        generator = SequenceUuidGenerator.create();
        for (int i = 1; i <= 1000000; ++i) {
            String str = StringUtils.leftPad("" + i, 12, "0");
            String uuidStr = generator.generateId();
            assertTrue(uuidStr.matches("^[0-9]{8}-[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{12}$"));
            assertEquals(UUID.fromString("00000000-0000-0000-0000-" + str), UUID.fromString(uuidStr));
        }
    }

}
