package com.enterprisemath.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;

/**
 * Test case to domain utilities.
 *
 * @author radek.hecl
 *
 */
public class DutTest {

    /**
     * Creates new instance.
     */
    public DutTest() {
    }

    /**
     * Test which proves that splitting collections into lists is working.
     */
    @Test
    public void testSplitToLists() {
        List<List<Integer>> res = null;
        List<List<Integer>> expected = null;

        res = Dut.splitToLists(null, 3);
        expected = new ArrayList<>();
        assertEquals(expected, res);

        res = Dut.splitToLists(Collections.emptyList(), 3);
        expected = new ArrayList<>();
        assertEquals(expected, res);

        res = Dut.splitToLists(Arrays.asList(1, 2, 3), 3);
        expected = Arrays.asList(Arrays.asList(1, 2, 3));
        assertEquals(expected, res);

        res = Dut.splitToLists(Arrays.asList(1, 2, 3, 4), 3);
        expected = Arrays.asList(Arrays.asList(1, 2, 3), Arrays.asList(4));
        assertEquals(expected, res);

        res = Dut.splitToLists(Arrays.asList(1, 2, 3, 4, 5, 6, 7), 3);
        expected = Arrays.asList(Arrays.asList(1, 2, 3), Arrays.asList(4, 5, 6), Arrays.asList(7));
        assertEquals(expected, res);

        res = Dut.splitToLists(Arrays.asList(1, 2, 3, 1, 2, 3, 1), 3);
        expected = Arrays.asList(Arrays.asList(1, 2, 3), Arrays.asList(1, 2, 3), Arrays.asList(1));
        assertEquals(expected, res);

        try {
            Dut.splitToLists(null, 0);
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            //
        }
    }

    /**
     * Test for date object copy.
     */
    @Test
    public void testCopyDate() {
        Date expected = new Date();
        Date current = Dut.copyDate(expected);
        assertEquals(expected, current);
        assertNotSame(expected, current);
    }

    /**
     * Test to prove method for safe equals is working.
     */
    @Test
    public void testSafeEquals() {
        assertTrue(Dut.safeEquals(null, null));
        assertFalse(Dut.safeEquals(null, 1));
        assertFalse(Dut.safeEquals(1, null));

        assertTrue(Dut.safeEquals(1, 1));
        assertTrue(Dut.safeEquals("hello", "hello"));
        assertFalse(Dut.safeEquals("hello", "nohello"));
        assertFalse(Dut.safeEquals(1, 1L));
    }

}
