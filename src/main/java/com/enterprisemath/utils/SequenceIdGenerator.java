package com.enterprisemath.utils;

/**
 * Id generator which produces ids in the sequence.
 *
 * @author radek.hecl
 */
public class SequenceIdGenerator implements IdGenerator {

    /**
     * Prefix.
     */
    private String prefix;

    /**
     * Next number.
     */
    private int next = 1;

    /**
     * Object for synchronization.
     */
    private final Object lock = new Object();

    /**
     * Creates new instance.
     *
     * @param builder builder object
     */
    private SequenceIdGenerator() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        Guard.notNull(prefix, "prefix cannot be null");
    }

    @Override
    public String generateId() {
        synchronized (lock) {
            String res = prefix + next;
            next = next + 1;
            return res;
        }
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param prefix prefix
     * @return created instance
     */
    public static SequenceIdGenerator create(String prefix) {
        SequenceIdGenerator res = new SequenceIdGenerator();
        res.prefix = prefix;
        res.guardInvariants();
        return res;
    }

    /**
     * Creates new instance.
     *
     * @return created instance
     */
    public static SequenceIdGenerator create() {
        return create("");
    }
}
