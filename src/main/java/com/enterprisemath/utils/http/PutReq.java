package com.enterprisemath.utils.http;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * Static methods related to PUT request.
 *
 * @author radek.hecl
 */
public class PutReq {

    /**
     * Prevents construction.
     */
    private PutReq() {
    }

    /**
     * Creates new instance with empty body.
     *
     * @param url URL
     * @return created request
     */
    public static HttpReq create(String url) {
        return new HttpReq.Builder().
                setUrl(url).
                setMethod(HttpReqMethod.PUT).
                build();
    }

    /**
     * Creates new request.
     *
     * @param url URL
     * @param headers headers
     * @param body request body
     * @return created request
     */
    public static HttpReq create(String url, List<HttpHeader> headers, byte[] body) {
        return new HttpReq.Builder().
                setUrl(url).
                setMethod(HttpReqMethod.PUT).
                setHeaders(headers).
                setBody(body).
                build();
    }

    /**
     * Creates new instance with URL encoded body.
     * This sets Content-Type header as application/x-www-form-urlencoded.
     *
     * @param url URL
     * @param param1Key key of the first parameter
     * @param param1Val value of the first parameter
     * @param others other parameters (must be in the key and value)
     * @return created request
     */
    public static HttpReq createUrlBody(String url, String param1Key, String param1Val, String... others) {
        try {
            StringBuilder urlbld = new StringBuilder();
            urlbld.append(param1Key);
            urlbld.append("=");
            urlbld.append(URLEncoder.encode(param1Val, "utf-8"));
            for (int i = 0; i < others.length; i = i + 2) {
                urlbld.append("&");
                urlbld.append(others[i]);
                urlbld.append("=");
                urlbld.append(URLEncoder.encode(others[i + 1], "utf-8"));
            }
            return new HttpReq.Builder().
                    setUrl(url).
                    setMethod(HttpReqMethod.PUT).
                    addHeader(HttpHeader.create("Content-Type", "application/x-www-form-urlencoded")).
                    setBody(urlbld.toString().getBytes("utf-8")).
                    build();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates new instance with string body.
     * This sets Content-Type header as application/json.
     *
     * @param url URL
     * @param json body text which is expected to be a JSON string
     * @return created request
     */
    public static HttpReq createJsonBody(String url, String json) {
        try {
            return new HttpReq.Builder().
                    setUrl(url).
                    setMethod(HttpReqMethod.PUT).
                    addHeader(HttpHeader.create("Content-Type", "application/json")).
                    setBody(json.getBytes("utf-8")).
                    build();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates new instance with string body.
     * First header is Content-Type and value is application/json. Specified headers are following.
     *
     * @param url URL
     * @param headers headers
     * @param json body text which is expected to be a JSON string
     * @return created request
     */
    public static HttpReq createJsonBody(String url, List<HttpHeader> headers, String json) {
        try {
            return new HttpReq.Builder().
                    setUrl(url).
                    setMethod(HttpReqMethod.PUT).
                    addHeader(HttpHeader.create("Content-Type", "application/json")).
                    addAllHeaders(headers).
                    setBody(json.getBytes("utf-8")).
                    build();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

}
