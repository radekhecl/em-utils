package com.enterprisemath.utils;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Test case which proves comparing.
 *
 * @author radek.hecl
 *
 */
public class PropertyStringComparatorTest {

    /**
     * Creates new instance.
     */
    public PropertyStringComparatorTest() {
    }

    /**
     * Tests the compare method.
     */
    @Test
    public void testCompare() {
        PropertyStringComparator comparator = PropertyStringComparator.create();

        assertEquals(0, comparator.compare("test", "test"));
        assertEquals(-1, comparator.compare("a", "z"));
        assertEquals(1, comparator.compare("z", "a"));

        assertEquals(-1, comparator.compare("A", "a"));
        assertEquals(1, comparator.compare("a", "A"));

        assertEquals(-1, comparator.compare("test.hello", "test.world"));
        assertEquals(1, comparator.compare("test.world", "test.hello"));

        assertEquals(-1, comparator.compare("items[1].id", "numItems"));
        assertEquals(1, comparator.compare("numItems", "items[1].id"));

        assertEquals(-1, comparator.compare("items[1]", "items[1].id"));
        assertEquals(1, comparator.compare("items[1].id", "items[1]"));

        assertEquals(-1, comparator.compare("items[0].price", "items[1].id"));
        assertEquals(1, comparator.compare("items[1].id", "items[0].price"));

        assertEquals(-1, comparator.compare("items[1].descriptions[0]", "items[1].descriptions[0003]"));
        assertEquals(1, comparator.compare("items[1].descriptions[0003]", "items[1].descriptions[0]"));

        assertEquals(-1, comparator.compare("items[0].descriptions[1]", "items[1].descriptions[0]"));
        assertEquals(1, comparator.compare("items[1].descriptions[0]", "items[0].descriptions[1]"));
    }

}
