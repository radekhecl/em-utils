package com.enterprisemath.utils.cache;

import java.util.NoSuchElementException;

/**
 * Defines interface for caching objects.
 * <p>
 * <strong>Note:</strong> Adding functions to this interface will <strong>NOT</strong> be
 * considered as breaking binary compatibility.
 * </p>
 *
 * @author radek.hecl
 */
public interface ObjectCache {

    /**
     * Puts object into cache. If object with the same key exists, then it is are overridden.
     *
     * @param <T> object type
     * @param key data key
     * @param object object
     */
    public <T> void put(String key, T object);

    /**
     * Removes the specified key.
     *
     * @param key key
     */
    public void remove(String key);

    /**
     * Retrieves object.
     *
     * @param <T> object type
     * @param clazz object class
     * @param key data key
     * @return retrieved object
     * @throws NoSuchElementException if key doesn't exists
     */
    public <T> T get(Class<T> clazz, String key);

    /**
     * Returns object in the non strict manner. Default value is
     * returned if key doesn't exists.
     *
     * @param <T> object type
     * @param clazz object class
     * @param key key
     * @param def default value returned if key doesn't exists
     * @return object or default value
     */
    public <T> T get(Class<T> clazz, String key, T def);

    /**
     * Returns whether entry exists under the specified key.
     *
     * @param key tested key
     * @return true if entry exists, false otherwise
     */
    public boolean contains(String key);

    /**
     * Clears the cache, all objects are removed.
     */
    public void clear();

}
