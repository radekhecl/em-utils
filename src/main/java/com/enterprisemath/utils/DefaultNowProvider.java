package com.enterprisemath.utils;

import java.time.Instant;
import java.util.Date;

/**
 * Default timestamp provider implementation.
 *
 * @author radek.hecl
 */
public class DefaultNowProvider implements NowProvider {

    /**
     * Creates new instance.
     */
    public DefaultNowProvider() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
    }

    @Override
    public Date date() {
        return new Date();
    }

    @Override
    public Instant instant() {
        return Instant.now();
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new default dates provider.
     *
     * @return created provider
     */
    public static DefaultNowProvider create() {
        DefaultNowProvider res = new DefaultNowProvider();
        res.guardInvariants();
        return res;
    }

}
