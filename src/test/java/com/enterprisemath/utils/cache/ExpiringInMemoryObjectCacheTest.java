package com.enterprisemath.utils.cache;

import java.io.IOException;
import java.util.NoSuchElementException;
import org.apache.commons.lang3.builder.ToStringBuilder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

/**
 * Test case which proves in memory object cache.
 *
 * @author radek.hecl
 */
public class ExpiringInMemoryObjectCacheTest {

    /**
     * Tested cache.
     */
    private ObjectCache cache;

    /**
     * Creates new instance.
     */
    public ExpiringInMemoryObjectCacheTest() {
    }

    /**
     * Sets up the test environment.
     *
     * @throws IOException if there is an error with files
     */
    @Before
    public void setUp() throws IOException {
        cache = ExpiringInMemoryObjectCache.create(1000, true);
    }

    /**
     * Tests put method.
     *
     * @throws IOException if there is an error with reading the created files
     */
    @Test
    public void testPut() throws IOException {
        String str1 = "string1";
        String str2 = "string2";

        cache.put("key1", str1);
        assertEquals(str1, cache.get(String.class, "key1"));
        cache.put("key1", str2);
        assertEquals(str2, cache.get(String.class, "key1"));
    }

    /**
     * Tests remove method.
     */
    @Test
    public void testRemove() {
        String str1 = "string1";
        String str2 = "string2";

        cache.put("key1", str1);
        cache.put("key2", str2);

        cache.remove("key1");
        assertFalse(cache.contains("key1"));
        assertEquals(str2, cache.get(String.class, "key2"));
    }

    /**
     * Tests returning object.
     */
    @Test
    public void testGet() {
        String str1 = "string1";
        String str2 = "string2";

        cache.put("key1", str1);
        cache.put("key2", str2);

        assertEquals(str1, cache.get(String.class, "key1"));
        assertEquals(str2, cache.get(String.class, "key2"));

        try {
            cache.get(String.class, "unknown");
            fail("exception expected");
        } catch (NoSuchElementException e) {
            // expected
        }

        assertEquals(str1, cache.get(String.class, "key1", "not_found"));
        assertEquals(str2, cache.get(String.class, "key2", "not_found"));
        assertEquals("not_found", cache.get(String.class, "unknown", "not_found"));
    }

    /**
     * Tests returning object when renewal on get request is turned on.
     */
    @Test
    public void testGet_renewOnGet() {
        String str1 = "string1";
        String str2 = "string2";

        cache.put("key1", str1);
        cache.put("key2", str2);

        try {
            Thread.sleep(550);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        assertEquals(str1, cache.get(String.class, "key1"));

        try {
            Thread.sleep(550);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        assertEquals(str1, cache.get(String.class, "key1"));

        try {
            cache.get(String.class, "key2");
            fail("exception expected");
        } catch (NoSuchElementException e) {
            // expected
        }
    }

    /**
     * Tests returning object when renewal on get request is turned off.
     */
    @Test
    public void testGet_notRenewOnGet() {
        cache = ExpiringInMemoryObjectCache.create(1000, false);
        String str1 = "string1";
        String str2 = "string2";

        cache.put("key1", str1);
        cache.put("key2", str2);

        try {
            Thread.sleep(550);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        assertEquals(str1, cache.get(String.class, "key1"));

        try {
            Thread.sleep(550);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        assertEquals("not_available", cache.get(String.class, "key1", "not_available"));

        try {
            cache.get(String.class, "key2");
            fail("exception expected");
        } catch (NoSuchElementException e) {
            // expected
        }
    }

    /**
     * Tests clear method.
     */
    @Test
    public void testClear() {
        String str1 = "string1";
        String str2 = "string2";

        cache.put("key1", str1);
        cache.put("key2", str2);
        cache.clear();

        assertFalse(cache.contains("key1"));
        assertFalse(cache.contains("key2"));
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
