package com.enterprisemath.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import org.apache.commons.lang3.StringUtils;

/**
 * Utilities to work with JSON. This class provides methods for encoding and decoding simple JSON structures.
 * Portion for escaping and parsing strings was taken from org.json library.
 * The library itself wasn't used as a dependency, because of conflicts which happened in the larger systems.
 *
 * @author radek.hecl
 */
public class Json {

    /**
     * Prevents construction.
     */
    private Json() {
    }

    /**
     * Encodes string map as a JSON string.
     * The result has format as {"hello" : "world", "hey" : "baby"}.
     *
     * @param data input data
     * @return encoded string
     */
    public static String encodeStringMap(Map<String, String> data) {
        SortedMap<String, String> sdata = Dut.copySortedMap(data);
        StringBuilder res = new StringBuilder();
        res.append("{");
        for (String key : sdata.keySet()) {
            res.append(quote(key));
            res.append(":");
            res.append(quote(sdata.get(key)));
            if (!key.equals(sdata.lastKey())) {
                res.append(",");
            }
        }
        res.append("}");
        return res.toString();
    }

    /**
     * Decodes string map from the JSON string.
     *
     * @param data input data in the {"hello" : "world", "hey" : "baby"} format
     * @return decoded map
     */
    public static Map<String, String> decodeStringMap(String data) {
        StringParser parser = new StringParser(StringUtils.trimToEmpty(data));
        String buf = parser.readCharacter();
        Guard.equals("{", buf, "wrong format, data must start with '{' character: %s", data);
        Map<String, String> res = new HashMap<>();
        while (!buf.equals("}")) {
            buf = parser.readTillStop("\"}");
            buf = parser.lookupCharacter();
            if (buf.equals("}")) {
                continue;
            }
            String key = readString(parser);
            parser.readTillStop(":");
            parser.readCharacter();
            parser.readTillStop("\"");
            String val = readString(parser);
            res.put(key, val);
            buf = parser.readTillStop(",}");
            buf = parser.readCharacter();
            if (StringUtils.isEmpty(buf)) {
                throw new IllegalArgumentException("wrong format, data must end with '}' character: " + data);
            }
        }
        return res;
    }

    /**
     * Encodes string list as a JSON string.
     * The result has format as ["hello", "baby"].
     *
     * @param data input data
     * @return encoded string
     */
    public static String encodeStringList(List<String> data) {
        StringBuilder res = new StringBuilder();
        res.append("[");
        for (int i = 0; i < data.size(); ++i) {
            res.append(quote(data.get(i)));
            if (i < data.size() - 1) {
                res.append(",");
            }
        }
        res.append("]");
        return res.toString();
    }

    /**
     * Decodes string list from the JSON string.
     *
     * @param data data in the ["hello", "baby"] format
     * @return decoded list
     */
    public static List<String> decodeStringList(String data) {
        StringParser parser = new StringParser(StringUtils.trimToEmpty(data));
        String buf = parser.readCharacter();
        Guard.equals("[", buf, "wrong format, data must start with '[' character: %s", data);
        List<String> res = new ArrayList<>();
        while (!buf.equals("]")) {
            buf = parser.readTillStop("\"]");
            buf = parser.lookupCharacter();
            if (buf.equals("]")) {
                continue;
            }
            res.add(readString(parser));
            buf = parser.readTillStop(",]");
            buf = parser.readCharacter();
            if (StringUtils.isEmpty(buf)) {
                throw new IllegalArgumentException("wrong format, data must end with ']' character: " + data);
            }
        }
        return res;
    }

    /**
     * Encodes integer list as a JSON string.
     * The result has format as [1, 2, 3].
     *
     * @param data input data
     * @return encoded string
     */
    public static String encodeIntList(List<Integer> data) {
        StringBuilder res = new StringBuilder();
        res.append("[");
        for (int i = 0; i < data.size(); ++i) {
            res.append(data.get(i));
            if (i < data.size() - 1) {
                res.append(",");
            }
        }
        res.append("]");
        return res.toString();
    }

    /**
     * Decodes integer list from the JSON string.
     *
     * @param data data in the [1, 2, 3] format
     * @return decoded list
     */
    public static List<Integer> decodeIntList(String data) {
        StringParser parser = new StringParser(StringUtils.trimToEmpty(data));
        String buf = parser.readCharacter();
        Guard.equals("[", buf, "wrong format, data must start with '[' character: %s", data);
        List<Integer> res = new ArrayList<>();
        while (!buf.equals("]")) {
            buf = StringUtils.trimToEmpty(parser.readTillStop(",]"));
            if (StringUtils.isNotEmpty(buf)) {
                res.add(Integer.valueOf(buf));
            }
            buf = parser.readCharacter();
            if (StringUtils.isEmpty(buf)) {
                throw new IllegalArgumentException("wrong format, data must end with ']' character: " + data);
            }
        }
        return res;
    }

    /**
     * Encodes integer list as a JSON string.
     * The result has format as [1, 2, 3].
     *
     * @param data input data
     * @return encoded string
     */
    public static String encodeFloatList(List<Float> data) {
        StringBuilder res = new StringBuilder();
        res.append("[");
        for (int i = 0; i < data.size(); ++i) {
            res.append(data.get(i));
            if (i < data.size() - 1) {
                res.append(",");
            }
        }
        res.append("]");
        return res.toString();
    }

    /**
     * Decodes float list from the JSON string.
     *
     * @param data data in the [1, 2, 3] format
     * @return decoded list
     */
    public static List<Float> decodeFloatList(String data) {
        StringParser parser = new StringParser(StringUtils.trimToEmpty(data));
        String buf = parser.readCharacter();
        Guard.equals("[", buf, "wrong format, data must start with '[' character: %s", data);
        List<Float> res = new ArrayList<>();
        while (!buf.equals("]")) {
            buf = StringUtils.trimToEmpty(parser.readTillStop(",]"));
            if (StringUtils.isNotEmpty(buf)) {
                res.add(Float.valueOf(buf));
            }
            buf = parser.readCharacter();
            if (StringUtils.isEmpty(buf)) {
                throw new IllegalArgumentException("wrong format, data must end with ']' character: " + data);
            }
        }
        return res;
    }

    /**
     * Quotes string. This means wrap it by " character and escape whatever needs to be escaped.
     *
     * @param input input string
     * @return quoted string
     */
    private static String quote(String input) {
        if (input == null || input.length() == 0) {
            return "\"\"";
        }
        StringParser parser = new StringParser(input);
        String buf = "";
        String pbuf = "";
        StringBuilder res = new StringBuilder();

        res.append('"');
        while (parser.hasNext()) {
            pbuf = buf;
            buf = parser.readCharacter();
            if ("\\".equals(buf) || "\"".equals(buf)) {
                res.append("\\");
                res.append(buf);
            }
            else if ("/".equals(buf)) {
                if ("<".equals(pbuf)) {
                    res.append("\\");
                }
                res.append(buf);
            }
            else if ("\b".equals(buf)) {
                res.append("\\b");
            }
            else if ("\t".equals(buf)) {
                res.append("\\t");
            }
            else if ("\n".equals(buf)) {
                res.append("\\n");
            }
            else if ("\f".equals(buf)) {
                res.append("\\f");
            }
            else if ("\r".equals(buf)) {
                res.append("\\r");
            }
            else {
                char c = buf.charAt(0);
                if (c < ' ' || (c >= '\u0080' && c < '\u00a0') || (c >= '\u2000' && c < '\u2100')) {
                    res.append("\\u");
                    String hex = Integer.toHexString(c);
                    res.append("0000", 0, 4 - hex.length());
                    res.append(hex);
                }
                else {
                    res.append(c);
                }

            }
        }
        res.append('"');
        return res.toString();
    }

    /**
     * Reads string from the given parser.
     * String is terminated by the " character. This method accounts for the escaping characters.
     *
     * @param parser parser, which is in the position right before the starting double quote, position is moved after the terminating double quote
     * @return consumed string
     */
    private static String readString(StringParser parser) {
        String buf = parser.readCharacter();
        StringBuilder res = new StringBuilder();
        for (;;) {
            buf = parser.readCharacter();
            if ("\n".equals(buf) || "\r".equals(buf)) {
                throw new IllegalArgumentException("unterminated string: " + parser.toString());
            }
            else if ("\\".equals(buf)) {
                buf = parser.readCharacter();
                if ("b".equals(buf)) {
                    res.append("\b");
                }
                else if ("t".equals(buf)) {
                    res.append("\t");
                }
                else if ("n".equals(buf)) {
                    res.append("\n");
                }
                else if ("f".equals(buf)) {
                    res.append("\f");
                }
                else if ("r".equals(buf)) {
                    res.append("\r");
                }
                else if ("u".equals(buf)) {
                    try {
                        String digits = parser.readCharacter() + parser.readCharacter() + parser.readCharacter() + parser.readCharacter();
                        res.append((char) Integer.parseInt(digits, 16));
                    } catch (NumberFormatException e) {
                        throw new IllegalArgumentException("illegal escape: " + parser.toString(), e);
                    }
                }
                else if ("\"".equals(buf) || "'".equals(buf) || "\\".equals(buf) || "/".equals(buf)) {
                    res.append(buf);
                }
                else {
                    throw new IllegalArgumentException("illegal escape: " + parser.toString());
                }
            }
            else {
                if ("\"".equals(buf)) {
                    break;
                }
                else {
                    res.append(buf);
                }
            }
        }
        return res.toString();
    }

}
