package com.enterprisemath.utils.cache;

import com.enterprisemath.utils.Guard;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Implementation of object cache which stores everything in memory and objects expires after
 * defined period of time.
 *
 * @author radek.hecl
 */
public class ExpiringInMemoryObjectCache implements ObjectCache {

    /**
     * Timeout for object expiration. In milliseconds.
     */
    private int expireTimeout;

    /**
     * Whether timeout should be renewed when calling get method.
     */
    private Boolean renewOnGet;

    /**
     * Buffer map.
     */
    private Map<String, List<Object>> buffer = new HashMap<>();

    /**
     * Object for locking.
     */
    private final Object lock = new Object();

    /**
     * Creates new instance.
     */
    private ExpiringInMemoryObjectCache() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        Guard.positive(expireTimeout, "expireTimeout must be positive");
        Guard.notNull(renewOnGet, "renewOnGet cannot be null");
    }

    @Override
    public void put(String key, Object object) {
        synchronized (lock) {
            expireOld();
            Long et = System.currentTimeMillis() + expireTimeout;
            List<Object> pair = new ArrayList<>(2);
            pair.add(et);
            pair.add(object);
            buffer.put(key, pair);
        }
    }

    @Override
    public void remove(String key) {
        synchronized (lock) {
            expireOld();
            buffer.remove(key);
        }
    }

    @Override
    public <T> T get(Class<T> clazz, String key) {
        synchronized (lock) {
            expireOld();
            if (!buffer.containsKey(key)) {
                throw new NoSuchElementException("element doesn't exists: key = " + key);
            }
            List<Object> pair = buffer.get(key);
            if (renewOnGet) {
                Long et = System.currentTimeMillis() + expireTimeout;
                pair.set(0, et);
            }
            return (T) pair.get(1);
        }
    }

    @Override
    public <T> T get(Class<T> clazz, String key, T def) {
        synchronized (lock) {
            expireOld();
            if (!buffer.containsKey(key)) {
                return def;
            }
            List<Object> pair = buffer.get(key);
            if (renewOnGet) {
                Long et = System.currentTimeMillis() + expireTimeout;
                pair.set(0, et);
            }
            return (T) pair.get(1);
        }
    }

    @Override
    public boolean contains(String key) {
        synchronized (lock) {
            expireOld();
            return buffer.containsKey(key);
        }
    }

    @Override
    public void clear() {
        synchronized (lock) {
            buffer.clear();
        }
    }

    /**
     * Expires old objects.
     */
    private void expireOld() {
        long ct = System.currentTimeMillis();
        Set<String> remove = new HashSet<>();
        for (String key : buffer.keySet()) {
            List<Object> pair = buffer.get(key);
            if ((Long) pair.get(0) < ct) {
                remove.add(key);
            }
        }
        for (String key : remove) {
            buffer.remove(key);
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param expireTimeout timeout for object expiration, in milliseconds
     * @param renewOnGet whether timeout should be renewed on get
     * @return created instance
     */
    public static ExpiringInMemoryObjectCache create(int expireTimeout, boolean renewOnGet) {
        ExpiringInMemoryObjectCache res = new ExpiringInMemoryObjectCache();
        res.expireTimeout = expireTimeout;
        res.renewOnGet = renewOnGet;
        res.guardInvariants();
        return res;
    }
}
