package com.enterprisemath.utils.http;

import com.enterprisemath.utils.Guard;

/**
 * HTTP header.
 *
 * @author radek.hecl
 */
public class HttpHeader {

    /**
     * Header name.
     */
    private String name;

    /**
     * Header value.
     */
    private String value;

    /**
     * Creates new instance.
     */
    private HttpHeader() {
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
        Guard.notEmpty(name, "name cannot be empty");
        Guard.notNull(value, "value cannot be null");
    }

    /**
     * Returns name.
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns value.
     *
     * @return value
     */
    public String getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        int h = 5381;
        h += (h << 5) + name.hashCode();
        h += (h << 5) + value.hashCode();
        return h;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof HttpHeader)) {
            return false;
        }
        HttpHeader another = (HttpHeader) obj;
        return name.equals(another.name) && value.equals(another.value);
    }

    @Override
    public String toString() {
        return name + "=" + value;
    }

    /**
     * Creates new instance.
     *
     * @param name name
     * @param value value
     * @return created instance
     */
    public static HttpHeader create(String name, String value) {
        HttpHeader res = new HttpHeader();
        res.name = name;
        res.value = value;
        res.guardInvariants();
        return res;
    }
}
