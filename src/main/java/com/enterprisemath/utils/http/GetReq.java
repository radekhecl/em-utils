package com.enterprisemath.utils.http;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * Static method related to GET requests.
 *
 * @author radek.hecl
 */
public class GetReq {

    /**
     * Prevents construction.
     */
    private GetReq() {
    }

    /**
     * Creates new instance.
     *
     * @param url url
     * @return created request
     */
    public static HttpReq create(String url) {
        return new HttpReq.Builder().
                setUrl(url).
                setMethod(HttpReqMethod.GET).
                build();
    }

    /**
     * Creates new instance.
     *
     * @param url url
     * @param param1Key key of the first parameter
     * @param param1Val value of the first parameter
     * @param others other parameters (must be in the key and value)
     * @return created request
     */
    public static HttpReq create(String url, String param1Key, String param1Val, String... others) {
        try {
            StringBuilder urlbld = new StringBuilder();
            urlbld.append(url);
            urlbld.append("?");
            urlbld.append(param1Key);
            urlbld.append("=");
            urlbld.append(URLEncoder.encode(param1Val, "utf-8"));
            for (int i = 0; i < others.length; i = i + 2) {
                urlbld.append("&");
                urlbld.append(others[i]);
                urlbld.append("=");
                urlbld.append(URLEncoder.encode(others[i + 1], "utf-8"));
            }
            return new HttpReq.Builder().
                    setUrl(urlbld.toString()).
                    setMethod(HttpReqMethod.GET).
                    build();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates new instance.
     *
     * @param url url
     * @param headers headers
     * @return created request
     */
    public static HttpReq create(String url, List<HttpHeader> headers) {
        return new HttpReq.Builder().
                setUrl(url).
                setMethod(HttpReqMethod.GET).
                setHeaders(headers).
                build();
    }

    /**
     * Creates new instance.
     *
     * @param url url
     * @param headers headers
     * @param param1Key key of the first parameter
     * @param param1Val value of the first parameter
     * @param others other parameters (must be in the key and value)
     * @return created request
     */
    public static HttpReq create(String url, List<HttpHeader> headers, String param1Key, String param1Val, String... others) {
        try {
            StringBuilder urlbld = new StringBuilder();
            urlbld.append(url);
            urlbld.append("?");
            urlbld.append(param1Key);
            urlbld.append("=");
            urlbld.append(URLEncoder.encode(param1Val, "utf-8"));
            for (int i = 0; i < others.length; i = i + 2) {
                urlbld.append("&");
                urlbld.append(others[i]);
                urlbld.append("=");
                urlbld.append(URLEncoder.encode(others[i + 1], "utf-8"));
            }
            return new HttpReq.Builder().
                    setUrl(urlbld.toString()).
                    setMethod(HttpReqMethod.GET).
                    setHeaders(headers).
                    build();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

}
