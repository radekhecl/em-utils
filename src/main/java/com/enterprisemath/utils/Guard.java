package com.enterprisemath.utils;

import java.util.Collection;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 * Collection of static methods for guarding the objects to satisfy constraints.
 *
 * @author radek.hecl
 */
public class Guard {

    /**
     * Prevents construction.
     */
    private Guard() {
    }

    /**
     * Guards object to be true.
     *
     * @param obj tested object
     * @param msg message in case of failure
     */
    public static void beTrue(boolean obj, String msg) {
        if (obj != true) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards object to be true.
     *
     * @param obj tested object
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void beTrue(boolean obj, String msg, Object... args) {
        if (obj != true) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards object to be false.
     *
     * @param obj tested object
     * @param msg message in case of failure
     */
    public static void beFalse(boolean obj, String msg) {
        if (obj != false) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards object to be false.
     *
     * @param obj tested object
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void beFalse(boolean obj, String msg, Object... args) {
        if (obj != false) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards object to be null.
     *
     * @param obj tested object
     * @param msg message in case of failure
     */
    public static void beNull(Object obj, String msg) {
        if (obj != null) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards object to be null.
     *
     * @param obj tested object
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void beNull(Object obj, String msg, Object... args) {
        if (obj != null) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards object to be not null.
     *
     * @param obj tested object
     * @param msg message in case of failure
     */
    public static void notNull(Object obj, String msg) {
        if (obj == null) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards object to be not null.
     *
     * @param obj tested object
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void notNull(Object obj, String msg, Object... args) {
        if (obj == null) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards two object to be equal. This is null safe, where two null objects are considered to be equal.
     *
     * @param obj1 object one
     * @param obj2 object two
     * @param msg message in case of failure
     */
    public static void equals(Object obj1, Object obj2, String msg) {
        if (obj1 == obj2) {
            return;
        }
        if (obj1 == null || !obj1.equals(obj2)) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards two object to be equal. This is null safe, where two null objects are considered to be equal.
     *
     * @param obj1 object one
     * @param obj2 object two
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void equals(Object obj1, Object obj2, String msg, Object... args) {
        if (obj1 == obj2) {
            return;
        }
        if (obj1 == null || !obj1.equals(obj2)) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards that specified number is 1, 2, 3...
     *
     * @param number tested number
     * @param msg message in case of failure
     */
    public static void positive(byte number, String msg) {
        if (number < 1) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards that specified number is 1, 2, 3...
     *
     * @param number tested number
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void positive(byte number, String msg, Object... args) {
        if (number < 1) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards that specified number is 1, 2, 3...
     *
     * @param number tested number
     * @param msg message in case of failure
     */
    public static void positive(short number, String msg) {
        if (number < 1) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards that specified number is 1, 2, 3...
     *
     * @param number tested number
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void positive(short number, String msg, Object... args) {
        if (number < 1) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards that specified number is 1, 2, 3...
     *
     * @param number tested number
     * @param msg message in case of failure
     */
    public static void positive(int number, String msg) {
        if (number < 1) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards that specified number is 1, 2, 3...
     *
     * @param number tested number
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void positive(int number, String msg, Object... args) {
        if (number < 1) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards that specified number is 1, 2, 3...
     *
     * @param number tested number
     * @param msg message in case of failure
     */
    public static void positive(long number, String msg) {
        if (number < 1L) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards that specified number is 1, 2, 3...
     *
     * @param number tested number
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void positive(long number, String msg, Object... args) {
        if (number < 1L) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards that specified number is positive.
     * NaN will cause an exception as well.
     *
     * @param number tested number
     * @param msg message in case of failure
     */
    public static void positive(float number, String msg) {
        if (number <= 0.0f || Float.isNaN(number)) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards that specified number is positive.
     * NaN will cause an exception as well.
     *
     * @param number tested number
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void positive(float number, String msg, Object... args) {
        if (number <= 0.0f || Float.isNaN(number)) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards that specified number is positive.
     * NaN will cause an exception as well.
     *
     * @param number tested number
     * @param msg message in case of failure
     */
    public static void positive(double number, String msg) {
        if (number <= 0.0 || Double.isNaN(number)) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards that specified number is positive.
     * NaN will cause an exception as well.
     *
     * @param number tested number
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void positive(double number, String msg, Object... args) {
        if (number <= 0.0 || Double.isNaN(number)) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards that specified number is 0, 1, 2, 3...
     *
     * @param number tested number
     * @param msg message in case of failure
     */
    public static void notNegative(byte number, String msg) {
        if (number < 0) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards that specified number is 0, 1, 2, 3...
     *
     * @param number tested number
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void notNegative(byte number, String msg, Object... args) {
        if (number < 0) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards that specified number is 0, 1, 2, 3...
     *
     * @param number tested number
     * @param msg message in case of failure
     */
    public static void notNegative(short number, String msg) {
        if (number < 0) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards that specified number is 0, 1, 2, 3...
     *
     * @param number tested number
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void notNegative(short number, String msg, Object... args) {
        if (number < 0) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards that specified number is 0, 1, 2, 3...
     *
     * @param number tested number
     * @param msg message in case of failure
     */
    public static void notNegative(int number, String msg) {
        if (number < 0) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards that specified number is 0, 1, 2, 3...
     *
     * @param number tested number
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void notNegative(int number, String msg, Object... args) {
        if (number < 0) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards that specified number is 0, 1, 2, 3...
     *
     * @param number tested number
     * @param msg message in case of failure
     */
    public static void notNegative(long number, String msg) {
        if (number < 0L) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards that specified number is 0, 1, 2, 3...
     *
     * @param number tested number
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void notNegative(long number, String msg, Object... args) {
        if (number < 0L) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards that specified number is not negative.
     * NaN will cause an exception as well.
     *
     * @param number tested number
     * @param msg message in case of failure
     */
    public static void notNegative(float number, String msg) {
        if (number < 0.0f || Float.isNaN(number)) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards that specified number is not negative.
     * NaN will cause an exception as well.
     *
     * @param number tested number
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void notNegative(float number, String msg, Object... args) {
        if (number < 0.0f || Float.isNaN(number)) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards that specified number is not negative.
     * NaN will cause an exception as well.
     *
     * @param number tested number
     * @param msg message in case of failure
     */
    public static void notNegative(double number, String msg) {
        if (number < 0.0 || Double.isNaN(number)) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards that specified number is not negative.
     * NaN will cause an exception as well.
     *
     * @param number tested number
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void notNegative(double number, String msg, Object... args) {
        if (number < 0.0 || Double.isNaN(number)) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards string to be empty. Empty means both null and empty string.
     *
     * @param str tested string
     * @param msg message to appear in the exception
     */
    public static void empty(String str, String msg) {
        if (StringUtils.isNotEmpty(str)) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards string to be empty. Empty means both null and empty string.
     *
     * @param str tested string
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void empty(String str, String msg, Object... args) {
        if (StringUtils.isNotEmpty(str)) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards that specified string is not empty. Empty means both null and empty string.
     *
     * @param str tested string
     * @param msg message in case of failure
     */
    public static void notEmpty(String str, String msg) {
        if (StringUtils.isEmpty(str)) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards that specified string is not empty. Empty means both null and empty string.
     *
     * @param str tested string
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void notEmpty(String str, String msg, Object... args) {
        if (StringUtils.isEmpty(str)) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards string to match the regular expression.
     *
     * @param str tested string, not null
     * @param regex regular expression which needs to be satisfied
     * @param msg message in case of failure
     */
    public static void match(String str, String regex, String msg) {
        if (str == null) {
            throw new IllegalArgumentException(msg);
        }
        if (!str.matches(regex)) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards string to match the regular expression.
     *
     * @param str tested string, not null
     * @param regex regular expression which needs to be satisfied
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void match(String str, String regex, String msg, Object... args) {
        if (str == null) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
        if (!str.matches(regex)) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards object to be inside the specified collection.
     *
     * @param obj tested object
     * @param allowed allowed object collection
     * @param msg message in case of failure
     */
    public static void in(Object obj, Collection<?> allowed, String msg) {
        if (!allowed.contains(obj)) {
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Guards object to be inside the specified collection.
     *
     * @param obj tested object
     * @param allowed allowed object collection
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void in(Object obj, Collection<?> allowed, String msg, Object... args) {
        if (!allowed.contains(obj)) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
    }

    /**
     * Guards collection to be not null and have not null element.
     *
     * @param <T> object type
     * @param collection tested collection
     * @param msg message in case of failure
     */
    public static <T> void notNullCollection(Collection<T> collection, String msg) {
        if (collection == null) {
            throw new IllegalArgumentException(msg);
        }
        for (T elm : collection) {
            if (elm == null) {
                throw new IllegalArgumentException(msg);
            }
        }
    }

    /**
     * Guards collection to be not null and have not null element.
     *
     * @param <T> object type
     * @param collection tested collection
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static <T> void notNullCollection(Collection<T> collection, String msg, Object... args) {
        if (collection == null) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
        for (T elm : collection) {
            if (elm == null) {
                throw new IllegalArgumentException(String.format(msg, args));
            }
        }
    }

    /**
     * Guards that collection is not null and also any element inside is not null or empty string.
     * Empty collection (size = 0) is fine.
     *
     * @param collection tested collection
     * @param msg message in case of failure
     */
    public static void notEmptyStringCollection(Collection<String> collection, String msg) {
        if (collection == null) {
            throw new IllegalArgumentException(msg);
        }
        for (String elm : collection) {
            if (StringUtils.isEmpty(elm)) {
                throw new IllegalArgumentException(msg);
            }
        }
    }

    /**
     * Guards that collection is not null and also any element inside is not null or empty string.
     * Empty collection (size = 0) is fine.
     *
     * @param collection tested collection
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static void notEmptyStringCollection(Collection<String> collection, String msg, Object... args) {
        if (collection == null) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
        for (String elm : collection) {
            if (StringUtils.isEmpty(elm)) {
                throw new IllegalArgumentException(String.format(msg, args));
            }
        }
    }

    /**
     * Guards that given map has all keys and values not null.
     *
     * @param <K> key type
     * @param <V> value type
     * @param map tested map
     * @param msg message in case of failure
     */
    public static <K, V> void notNullMap(Map<K, V> map, String msg) {
        if (map == null) {
            throw new IllegalArgumentException(msg);
        }
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (entry.getKey() == null) {
                throw new IllegalArgumentException(msg);
            }
            if (entry.getValue() == null) {
                throw new IllegalArgumentException(msg);
            }
        }
    }

    /**
     * Guards that given map has all keys and values not null.
     *
     * @param <K> key type
     * @param <V> value type
     * @param map tested map
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static <K, V> void notNullMap(Map<K, V> map, String msg, Object... args) {
        if (map == null) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (entry.getKey() == null) {
                throw new IllegalArgumentException(String.format(msg, args));
            }
            if (entry.getValue() == null) {
                throw new IllegalArgumentException(String.format(msg, args));
            }
        }
    }

    /**
     * Guards that given map has all keys not empty and all values not null.
     *
     * @param <V> value type
     * @param map tested map
     * @param msg message in case of failure
     */
    public static <V> void notEmptyNullMap(Map<String, V> map, String msg) {
        if (map == null) {
            throw new IllegalArgumentException(msg);
        }
        for (Map.Entry<String, V> entry : map.entrySet()) {
            if (StringUtils.isEmpty(entry.getKey())) {
                throw new IllegalArgumentException(msg);
            }
            if (entry.getValue() == null) {
                throw new IllegalArgumentException(msg);
            }
        }
    }

    /**
     * Guards that given map has all keys not empty and all values not null.
     *
     * @param <V> value type
     * @param map tested map
     * @param msg message formatting string in case of failure
     * @param args formatting string arguments
     */
    public static <V> void notEmptyNullMap(Map<String, V> map, String msg, Object... args) {
        if (map == null) {
            throw new IllegalArgumentException(String.format(msg, args));
        }
        for (Map.Entry<String, V> entry : map.entrySet()) {
            if (StringUtils.isEmpty(entry.getKey())) {
                throw new IllegalArgumentException(String.format(msg, args));
            }
            if (entry.getValue() == null) {
                throw new IllegalArgumentException(String.format(msg, args));
            }
        }
    }

}
