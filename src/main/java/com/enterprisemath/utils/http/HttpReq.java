package com.enterprisemath.utils.http;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * HTTP request.
 *
 * @author radek.hecl
 */
public class HttpReq {

    /**
     * Ignore certs instruction key.
     */
    public static final String IGNORE_CERTS = "ignoreCerts";

    /**
     * Read timeout instruction key.
     */
    public static final String READ_TIMEOUT = "readTimeout";

    /**
     * Builder object.
     */
    public static class Builder {

        /**
         * Url.
         */
        private String url;

        /**
         * Method.
         */
        private HttpReqMethod method;

        /**
         * Headers.
         */
        private List<HttpHeader> headers = new ArrayList<>();

        /**
         * Body.
         */
        private byte[] body;

        /**
         * Instructions.
         */
        private Map<String, String> instructions = new HashMap<>();

        /**
         * Sets URL.
         *
         * @param url URL
         * @return this instance
         */
        public Builder setUrl(String url) {
            this.url = url;
            return this;
        }

        /**
         * Sets method.
         *
         * @param method method
         * @return this instance
         */
        public Builder setMethod(HttpReqMethod method) {
            this.method = method;
            return this;
        }

        /**
         * Sets headers.
         *
         * @param headers headers
         * @return this instance
         */
        public Builder setHeaders(Collection<HttpHeader> headers) {
            this.headers = Dut.copyList(headers);
            return this;
        }

        /**
         * Adds header.
         *
         * @param header header
         * @return this instance
         */
        public Builder addHeader(HttpHeader header) {
            this.headers.add(header);
            return this;
        }

        /**
         * Adds all headers.
         *
         * @param headers headers to be added
         * @return this instance
         */
        public Builder addAllHeaders(Collection<HttpHeader> headers) {
            this.headers.addAll(headers);
            return this;
        }

        /**
         * Sets body.
         *
         * @param body body
         * @return this instance
         */
        public Builder setBody(byte[] body) {
            this.body = body == null ? null : Arrays.copyOf(body, body.length);
            return this;
        }

        /**
         * Sets instructions.
         *
         * @param instructions instructions
         * @return this instance
         */
        public Builder setInstructions(Map<String, String> instructions) {
            this.instructions = Dut.copyMap(instructions);
            return this;
        }

        /**
         * Puts the new instruction into request.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder putInstruction(String key, String value) {
            this.instructions.put(key, value);
            return this;
        }

        /**
         * Builds the result object.
         *
         * @return created object
         */
        public HttpReq build() {
            return new HttpReq(this);
        }
    }

    /**
     * Url.
     */
    private String url;

    /**
     * Method.
     */
    private HttpReqMethod method;

    /**
     * Headers.
     */
    private List<HttpHeader> headers;

    /**
     * Body.
     */
    private byte[] body;

    /**
     * Instructions.
     */
    private Map<String, String> instructions;

    /**
     * Creates new instance.
     *
     * @param builder builder object
     */
    private HttpReq(Builder builder) {
        url = builder.url;
        method = builder.method;
        headers = Dut.copyImmutableList(builder.headers);
        byte[] bbody = builder.body;
        body = bbody == null ? null : Arrays.copyOf(bbody, bbody.length);
        instructions = Dut.copyImmutableMap(builder.instructions);
        guardInvariants();
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
        Guard.notEmpty(url, "url cannot be empty");
        Guard.notNull(method, "method cannot be null");
        Guard.notNullCollection(headers, "headers cannot have null element");
        Guard.notEmptyNullMap(instructions, "instructions cannot have empty key or null value");
    }

    /**
     * Returns URL.
     *
     * @return URL
     */
    public String getUrl() {
        return url;
    }

    /**
     * Returns HTTP method.
     *
     * @return HTTP method
     */
    public HttpReqMethod getMethod() {
        return method;
    }

    /**
     * Returns headers.
     *
     * @return headers
     */
    public List<HttpHeader> getHeaders() {
        return headers;
    }

    /**
     * Returns body. This can return null if request does not have body
     *
     * @return body bytes or null if request doesn't have body
     */
    public byte[] getBody() {
        if (body == null) {
            return null;
        }
        return Arrays.copyOf(body, body.length);
    }

    /**
     * Returns the additional instructions for fulfilling the request.
     * These might be the things like ignoring digital certificates, connection timeout, or caching.
     * Common instruction keys are defined as a constants within this class.
     * Different implementations of connectors might support different instructions.
     * This allows to override the default connector settings for the specified request.
     *
     * @return additional instructions for fulfilling the request
     */
    public Map<String, String> getInstructions() {
        return instructions;
    }

    @Override
    public int hashCode() {
        return Dut.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return Dut.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

}
