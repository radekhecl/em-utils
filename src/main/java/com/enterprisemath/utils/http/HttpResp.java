package com.enterprisemath.utils.http;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.zip.GZIPInputStream;
import org.apache.commons.lang3.StringUtils;

/**
 * HTTP response.
 *
 * @author radek.hecl
 */
public class HttpResp {

    /**
     * Status code.
     */
    private int code;

    /**
     * Headers.
     */
    private List<HttpHeader> headers;

    /**
     * Body.
     */
    private byte[] body;

    /**
     * Creates new instance.
     */
    private HttpResp() {
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
        Guard.notNullCollection(headers, "headers cannot have null element");
        Guard.notNull(body, "body cannot be null");
    }

    /**
     * Returns whether response is 2XX (success), or not.
     *
     * @return true if response is 2XX, false otherwise
     */
    public boolean is2XX() {
        return code >= 200 && code < 300;
    }

    /**
     * Returns status code.
     *
     * @return status code
     */
    public int getCode() {
        return code;
    }

    /**
     * Returns headers.
     *
     * @return headers
     */
    public List<HttpHeader> getHeaders() {
        return headers;
    }

    /**
     * Returns the value of the header with the specified name.
     * If more than one one header with the specified name exists, then the value of the first one will be returned.
     *
     * @param name case insensitive header name
     * @return value of the first header
     */
    public String getHeader(String name) {
        name = name.toLowerCase(Locale.ROOT);
        for (HttpHeader header : headers) {
            if (header.getName().toLowerCase(Locale.ROOT).equals(name)) {
                return header.getValue();
            }
        }
        throw new IndexOutOfBoundsException("header " + name + " is not defined");
    }

    /**
     * Returns the value of the header with the specified name.
     * If more than one one header with the specified name exists, then the value of the first one will be returned.
     * If no header with the specified name exists, then returns default value.
     *
     * @param name header name, which is case insensitive
     * @param def default value which will be used if there is no header with the specified name
     * @return value of the first header
     */
    public String getHeader(String name, String def) {
        name = name.toLowerCase(Locale.ROOT);
        for (HttpHeader header : headers) {
            if (header.getName().toLowerCase(Locale.ROOT).equals(name)) {
                return header.getValue();
            }
        }
        return def;
    }

    /**
     * Returns body as it was received.
     *
     * @return body body as it was received
     */
    public byte[] getBody() {
        return Arrays.copyOf(body, body.length);
    }

    /**
     * Returns the body which is guaranteed to be decompressed.
     *
     * @return body which is guaranteed to be decompressed
     */
    public byte[] getDecompressedBody() {
        String encoding = getHeader("content-encoding", "identity").toLowerCase(Locale.ROOT);
        if (encoding.equals("identity")) {
            return Arrays.copyOf(body, body.length);
        }
        else if (encoding.equals("gzip")) {
            try (ByteArrayInputStream bis = new ByteArrayInputStream(body);
                    GZIPInputStream gis = new GZIPInputStream(bis);
                    ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
                byte[] buffer = new byte[1024];
                int len;
                while ((len = gis.read(buffer)) != -1) {
                    bos.write(buffer, 0, len);
                }
                return bos.toByteArray();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        else {
            throw new RuntimeException("unknown content-encoding: " + encoding);
        }
    }

    /**
     * Returns the body as a string. This method performs operations like
     * unzip and character encoding detection.
     *
     * @return string body as a text
     */
    public String getBodyString() {
        try {
            String charset = getCharset();
            if (charset == null) {
                charset = "utf-8";
            }
            byte[] decompressed = getDecompressedBody();
            return new String(decompressed, charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates input stream of the response body.
     * It is responsibility of the caller to close this.
     *
     * @return response body input stream, caller is responsible for closing it
     */
    public InputStream createBodyStream() {
        return new ByteArrayInputStream(body);
    }

    /**
     * Returns charset. Might be null if it's not possible to read it from headers.
     *
     * @return charst or null if it's not possilbe to read it from headers
     */
    private String getCharset() {
        String ct = getHeader("content-type", null);
        if (ct == null) {
            return null;
        }
        String[] ctparts = ct.split(";");
        for (String ctpart : ctparts) {
            ctpart = StringUtils.trim(ctpart);
            if (ctpart.toLowerCase(Locale.ROOT).startsWith("charset")) {
                String[] charsetparts = ctpart.split("=", 2);
                return charsetparts[1].trim().toLowerCase(Locale.ROOT);
            }
        }
        return null;
    }

    @Override
    public int hashCode() {
        return Dut.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return Dut.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return new StringBuilder().
                append("code=").append(code).append("; ").
                append("headers=").append(headers).append("; ").
                append("length=").append(body.length).
                toString();
    }

    /**
     * Creates new instance.
     *
     * @param code status code
     * @param headers headers
     * @param body body
     * @return created response
     */
    public static HttpResp create(int code, List<HttpHeader> headers, byte[] body) {
        HttpResp res = new HttpResp();
        res.code = code;
        res.headers = Dut.copyImmutableList(headers);
        res.body = body == null ? new byte[0] : Arrays.copyOf(body, body.length);
        res.guardInvariants();
        return res;
    }

}
