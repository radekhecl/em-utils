package com.enterprisemath.utils;

import java.util.Arrays;
import java.util.Collections;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Test case which proves JSON utilities.
 *
 * @author radek.hecl
 */
public class JsonTest {

    /**
     * Creates new instance.
     */
    public JsonTest() {
    }

    /**
     * Tests encoding and decoding the string map.
     */
    @Test
    public void testStringMap() {
        assertEquals("{}", Json.encodeStringMap(Collections.emptyMap()));
        assertEquals("{\"hello\":\"baby\"}", Json.encodeStringMap(Dut.map("hello", "baby")));
        assertEquals("{\"one\":\"1\",\"two\":\"2\"}", Json.encodeStringMap(Dut.map("one", "1", "two", "2")));
        assertEquals("{\"new line \\n here\":\"value \\n new line\",\"quotes \\\" here\":\"value \\\" quotes\"}",
                Json.encodeStringMap(Dut.map("quotes \" here", "value \" quotes", "new line \n here", "value \n new line")));

        assertEquals(Collections.emptyMap(), Json.decodeStringMap("{}"));
        assertEquals(Collections.emptyMap(), Json.decodeStringMap("{ }"));
        assertEquals(Collections.emptyMap(), Json.decodeStringMap("{\n}"));
        assertEquals(Dut.map("hello", "baby"), Json.decodeStringMap("{\"hello\":\"baby\"}"));
        assertEquals(Dut.map("hello", "baby"), Json.decodeStringMap("{ \"hello\" : \"baby\" }"));
        assertEquals(Dut.map("hello", "baby"), Json.decodeStringMap("{\n  \"hello\" : \"baby\"\n}"));
        assertEquals(Dut.map("quotes \" here", "value \" quotes", "new line \n here", "value \n new line"),
                Json.decodeStringMap("{\"new line \\n here\":\"value \\n new line\",\"quotes \\\" here\":\"value \\\" quotes\"}"));
        assertEquals(Dut.map("quotes \" here", "value \" quotes", "new line \n here", "value \n new line"),
                Json.decodeStringMap("{\n  \"new line \\n here\" : \"value \\n new line\",\n  \"quotes \\\" here\" : \"value \\\" quotes\"\n}"));
    }

    /**
     * Tests encoding and decoding the string list.
     */
    @Test
    public void testStringList() {
        // encode
        assertEquals("[]", Json.encodeStringList(Collections.emptyList()));
        assertEquals("[\"hello\"]", Json.encodeStringList(Arrays.asList("hello")));
        assertEquals("[\"hello\",\"baby\",\"\"]", Json.encodeStringList(Arrays.asList("hello", "baby", "")));
        assertEquals("[\"quotes \\\" here\",\"new line \\n here\"]", Json.encodeStringList(Arrays.asList("quotes \" here", "new line \n here")));

        // decode
        assertEquals(Collections.emptyList(), Json.decodeStringList("[]"));
        assertEquals(Collections.emptyList(), Json.decodeStringList("[ ]"));
        assertEquals(Collections.emptyList(), Json.decodeStringList("[\n]"));
        assertEquals(Arrays.asList("hello"), Json.decodeStringList("[\"hello\"]"));
        assertEquals(Arrays.asList("hello", "baby", ""), Json.decodeStringList("[\"hello\",\"baby\",\"\"]"));
        assertEquals(Arrays.asList("hello", "baby", ""), Json.decodeStringList("[\"hello\", \"baby\", \"\"]"));
        assertEquals(Arrays.asList("hello", "baby", ""), Json.decodeStringList("[\n  \"hello\",\n  \"baby\",\n  \"\"\n]"));
        assertEquals(Arrays.asList("quotes \" here", "new line \n here"), Json.decodeStringList("[\"quotes \\\" here\",\"new line \\n here\"]"));
    }

    /**
     * Tests encoding and decoding the integer list.
     */
    @Test
    public void testIntList() {
        // encode
        assertEquals("[]", Json.encodeIntList(Collections.emptyList()));
        assertEquals("[1,2,3]", Json.encodeIntList(Dut.list(1, 2, 3)));
        assertEquals("[1,2,-3]", Json.encodeIntList(Dut.list(1, 2, -3)));

        // decode
        assertEquals(Collections.emptyList(), Json.decodeIntList("[]"));
        assertEquals(Dut.list(1, 2, 3), Json.decodeIntList("[1,2,3]"));
        assertEquals(Dut.list(1, 2, 3), Json.decodeIntList("[1, 2, 3]"));
        assertEquals(Dut.list(1, 2, 3), Json.decodeIntList("[1,\n2,\n3]"));
        assertEquals(Dut.list(123, -212), Json.decodeIntList("[123, -212]"));
    }

    /**
     * Tests encoding and decoding the float list.
     */
    @Test
    public void testFloatList() {
        // encode
        assertEquals("[]", Json.encodeFloatList(Collections.emptyList()));
        assertEquals("[1.5,2.3E-5,1.4]", Json.encodeFloatList(Dut.list(1.5f, 2.3e-5f, 1.4f)));
        assertEquals("[-1.5,-2.3E-5,-1.4]", Json.encodeFloatList(Dut.list(-1.5f, -2.3e-5f, -1.4f)));

        // decode
        assertEquals(Collections.emptyList(), Json.decodeFloatList("[]"));
        assertEquals(Dut.list(1.5f, 2.3e-5f, 3.123e13f), Json.decodeFloatList("[1.5,2.3e-5,3.123e13]"));
        assertEquals(Dut.list(1.5f, 2.3e-5f, 3.123e13f), Json.decodeFloatList("[1.5,2.3E-5,3.123E13]"));
        assertEquals(Dut.list(1.5f, 2.3e-5f, 3.123e13f), Json.decodeFloatList("[1.5, 2.3e-5, 3.123e13]"));
        assertEquals(Dut.list(1.5f, 2.3e-5f, 3.123e13f), Json.decodeFloatList("[1.5,\n2.3e-5,\n3.123e13]"));
        assertEquals(Dut.list(-1.5f, -2.3e-5f, -3.123e13f), Json.decodeFloatList("[-1.5, -2.3e-5, -3.123e13]"));
    }
}
