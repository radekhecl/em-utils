package com.enterprisemath.utils;

import java.time.DayOfWeek;
import java.time.Month;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import static junit.framework.Assert.assertEquals;
import org.junit.Test;

/**
 * Test case which proves utility methods for operation with dates are working.
 *
 * @author radek.hecl
 *
 */
public class DatesTest {

    /**
     * Creates new instance.
     */
    public DatesTest() {
    }

    /**
     * Test for date creation.
     */
    @Test
    public void testCreateDate() {
        Calendar calendar = null;
        Date expected = null;
        Date res = null;

        // beginning of the year
        calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, 1992);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DATE, 1);
        expected = calendar.getTime();
        res = Dates.createDate(1992, Month.JANUARY, 1);
        assertEquals(expected, res);

        // end of the year
        calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, 1992);
        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.DATE, 31);
        expected = calendar.getTime();
        res = Dates.createDate(1992, Month.DECEMBER, 31);
        assertEquals(expected, res);
    }

    /**
     * Test for time creation.
     */
    @Test
    public void testCreateTime() {
        Calendar calendar = null;
        Date expected = null;
        Date res = null;

        // beginning of the year
        calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, 1992);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        expected = calendar.getTime();
        res = Dates.createTime(1992, Month.JANUARY, 1, 0, 0, 0);
        assertEquals(expected, res);

        // end of the year
        calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, 1992);
        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.DATE, 31);
        calendar.set(Calendar.HOUR, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        expected = calendar.getTime();
        res = Dates.createTime(1992, Month.DECEMBER, 31, 23, 59, 59);
        assertEquals(expected, res);

        // millisecond
        calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, 1992);
        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.DATE, 31);
        calendar.set(Calendar.HOUR, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        expected = calendar.getTime();
        res = Dates.createTime(1992, Month.DECEMBER, 31, 23, 59, 59, 999);
        assertEquals(expected, res);

    }

    /**
     * Test to get how many minutes passed since start of the day.
     */
    @Test
    public void testGetMinuteOfDay() {
        int res = -1;

        res = Dates.getMinuteOfDay(Dates.createTime(2012, Month.JANUARY, 1, 0, 0, 0));
        assertEquals(0, res);

        res = Dates.getMinuteOfDay(Dates.createTime(2012, Month.JANUARY, 1, 0, 0, 14));
        assertEquals(0, res);

        res = Dates.getMinuteOfDay(Dates.createTime(2012, Month.JANUARY, 1, 0, 59, 14));
        assertEquals(59, res);

        res = Dates.getMinuteOfDay(Dates.createTime(2012, Month.JANUARY, 1, 1, 0, 35));
        assertEquals(60, res);

        res = Dates.getMinuteOfDay(Dates.createTime(2012, Month.JANUARY, 1, 1, 12, 35));
        assertEquals(72, res);

        res = Dates.getMinuteOfDay(Dates.createTime(2012, Month.JANUARY, 1, 23, 59, 59));
        assertEquals(1439, res);
    }

    /**
     * Test to get how many hours passed since start of the day.
     */
    @Test
    public void testGetHourOfDay() {
        int res = -1;

        res = Dates.getHourOfDay(Dates.createTime(2012, Month.JANUARY, 1, 0, 0, 0));
        assertEquals(0, res);
        res = Dates.getHourOfDay(Dates.createTime(2012, Month.JANUARY, 1, 0, 59, 59));
        assertEquals(0, res);
        res = Dates.getHourOfDay(Dates.createTime(2012, Month.JANUARY, 1, 1, 30, 0));
        assertEquals(1, res);
        res = Dates.getHourOfDay(Dates.createTime(2012, Month.JANUARY, 1, 6, 30, 0));
        assertEquals(6, res);
        res = Dates.getHourOfDay(Dates.createTime(2012, Month.JANUARY, 1, 16, 30, 0));
        assertEquals(16, res);
        res = Dates.getHourOfDay(Dates.createTime(2012, Month.JANUARY, 1, 23, 30, 0));
        assertEquals(23, res);
    }

    /**
     * Test for method to get what day is it from the beginning of the year.
     */
    @Test
    public void testGetDayOfYear() {
        // 2011
        assertEquals(1, Dates.getDayOfYear(Dates.createDate(2011, Month.JANUARY, 1)));
        assertEquals(31, Dates.getDayOfYear(Dates.createDate(2011, Month.JANUARY, 31)));
        assertEquals(32, Dates.getDayOfYear(Dates.createDate(2011, Month.FEBRUARY, 1)));
        assertEquals(59, Dates.getDayOfYear(Dates.createDate(2011, Month.FEBRUARY, 28)));
        assertEquals(60, Dates.getDayOfYear(Dates.createDate(2011, Month.MARCH, 1)));
        assertEquals(90, Dates.getDayOfYear(Dates.createDate(2011, Month.MARCH, 31)));
        assertEquals(91, Dates.getDayOfYear(Dates.createDate(2011, Month.APRIL, 1)));
        assertEquals(120, Dates.getDayOfYear(Dates.createDate(2011, Month.APRIL, 30)));
        assertEquals(121, Dates.getDayOfYear(Dates.createDate(2011, Month.MAY, 1)));
        assertEquals(151, Dates.getDayOfYear(Dates.createDate(2011, Month.MAY, 31)));
        assertEquals(152, Dates.getDayOfYear(Dates.createDate(2011, Month.JUNE, 1)));
        assertEquals(181, Dates.getDayOfYear(Dates.createDate(2011, Month.JUNE, 30)));
        assertEquals(182, Dates.getDayOfYear(Dates.createDate(2011, Month.JULY, 1)));
        assertEquals(212, Dates.getDayOfYear(Dates.createDate(2011, Month.JULY, 31)));
        assertEquals(213, Dates.getDayOfYear(Dates.createDate(2011, Month.AUGUST, 1)));
        assertEquals(243, Dates.getDayOfYear(Dates.createDate(2011, Month.AUGUST, 31)));
        assertEquals(244, Dates.getDayOfYear(Dates.createDate(2011, Month.SEPTEMBER, 1)));
        assertEquals(273, Dates.getDayOfYear(Dates.createDate(2011, Month.SEPTEMBER, 30)));
        assertEquals(274, Dates.getDayOfYear(Dates.createDate(2011, Month.OCTOBER, 1)));
        assertEquals(304, Dates.getDayOfYear(Dates.createDate(2011, Month.OCTOBER, 31)));
        assertEquals(305, Dates.getDayOfYear(Dates.createDate(2011, Month.NOVEMBER, 1)));
        assertEquals(334, Dates.getDayOfYear(Dates.createDate(2011, Month.NOVEMBER, 30)));
        assertEquals(335, Dates.getDayOfYear(Dates.createDate(2011, Month.DECEMBER, 1)));
        assertEquals(365, Dates.getDayOfYear(Dates.createDate(2011, Month.DECEMBER, 31)));
        // 2012
        assertEquals(1, Dates.getDayOfYear(Dates.createDate(2012, Month.JANUARY, 1)));
        assertEquals(31, Dates.getDayOfYear(Dates.createDate(2012, Month.JANUARY, 31)));
        assertEquals(32, Dates.getDayOfYear(Dates.createDate(2012, Month.FEBRUARY, 1)));
        assertEquals(60, Dates.getDayOfYear(Dates.createDate(2012, Month.FEBRUARY, 29)));
        assertEquals(61, Dates.getDayOfYear(Dates.createDate(2012, Month.MARCH, 1)));
        assertEquals(91, Dates.getDayOfYear(Dates.createDate(2012, Month.MARCH, 31)));
        assertEquals(92, Dates.getDayOfYear(Dates.createDate(2012, Month.APRIL, 1)));
        assertEquals(121, Dates.getDayOfYear(Dates.createDate(2012, Month.APRIL, 30)));
        assertEquals(122, Dates.getDayOfYear(Dates.createDate(2012, Month.MAY, 1)));
        assertEquals(152, Dates.getDayOfYear(Dates.createDate(2012, Month.MAY, 31)));
        assertEquals(153, Dates.getDayOfYear(Dates.createDate(2012, Month.JUNE, 1)));
        assertEquals(182, Dates.getDayOfYear(Dates.createDate(2012, Month.JUNE, 30)));
        assertEquals(183, Dates.getDayOfYear(Dates.createDate(2012, Month.JULY, 1)));
        assertEquals(213, Dates.getDayOfYear(Dates.createDate(2012, Month.JULY, 31)));
        assertEquals(214, Dates.getDayOfYear(Dates.createDate(2012, Month.AUGUST, 1)));
        assertEquals(244, Dates.getDayOfYear(Dates.createDate(2012, Month.AUGUST, 31)));
        assertEquals(245, Dates.getDayOfYear(Dates.createDate(2012, Month.SEPTEMBER, 1)));
        assertEquals(274, Dates.getDayOfYear(Dates.createDate(2012, Month.SEPTEMBER, 30)));
        assertEquals(275, Dates.getDayOfYear(Dates.createDate(2012, Month.OCTOBER, 1)));
        assertEquals(305, Dates.getDayOfYear(Dates.createDate(2012, Month.OCTOBER, 31)));
        assertEquals(306, Dates.getDayOfYear(Dates.createDate(2012, Month.NOVEMBER, 1)));
        assertEquals(335, Dates.getDayOfYear(Dates.createDate(2012, Month.NOVEMBER, 30)));
        assertEquals(336, Dates.getDayOfYear(Dates.createDate(2012, Month.DECEMBER, 1)));
        assertEquals(366, Dates.getDayOfYear(Dates.createDate(2012, Month.DECEMBER, 31)));
    }

    /**
     * Tests year retrieving.
     */
    @Test
    public void testGetYear() {
        assertEquals(2014, Dates.getYear(Dates.createDate(2014, Month.JANUARY, 1)));
        assertEquals(2016, Dates.getYear(Dates.createDate(2016, Month.DECEMBER, 31)));
    }

    /**
     * Tests month retrieving.
     */
    @Test
    public void testGetMonth() {
        assertEquals(Month.JANUARY, Dates.getMonth(Dates.createDate(2014, Month.JANUARY, 1)));
        assertEquals(Month.FEBRUARY, Dates.getMonth(Dates.createDate(2014, Month.FEBRUARY, 1)));
        assertEquals(Month.MARCH, Dates.getMonth(Dates.createDate(2014, Month.MARCH, 1)));
        assertEquals(Month.APRIL, Dates.getMonth(Dates.createDate(2014, Month.APRIL, 1)));
        assertEquals(Month.MAY, Dates.getMonth(Dates.createDate(2014, Month.MAY, 1)));
        assertEquals(Month.JUNE, Dates.getMonth(Dates.createDate(2014, Month.JUNE, 1)));
        assertEquals(Month.JULY, Dates.getMonth(Dates.createDate(2014, Month.JULY, 1)));
        assertEquals(Month.AUGUST, Dates.getMonth(Dates.createDate(2014, Month.AUGUST, 1)));
        assertEquals(Month.SEPTEMBER, Dates.getMonth(Dates.createDate(2014, Month.SEPTEMBER, 1)));
        assertEquals(Month.OCTOBER, Dates.getMonth(Dates.createDate(2014, Month.OCTOBER, 1)));
        assertEquals(Month.NOVEMBER, Dates.getMonth(Dates.createDate(2014, Month.NOVEMBER, 1)));
        assertEquals(Month.DECEMBER, Dates.getMonth(Dates.createDate(2014, Month.DECEMBER, 1)));
    }

    /**
     * Tests year retrieving.
     */
    @Test
    public void testGetDayOfMonth() {
        assertEquals(1, Dates.getDayOfMonth(Dates.createDate(2014, Month.JANUARY, 1)));
        assertEquals(29, Dates.getDayOfMonth(Dates.createDate(2016, Month.FEBRUARY, 29)));
        assertEquals(31, Dates.getDayOfMonth(Dates.createDate(2016, Month.DECEMBER, 31)));
    }

    /**
     * Tests day of the week retrieving.
     */
    @Test
    public void testGetDayOfWeek() {
        assertEquals(DayOfWeek.WEDNESDAY, Dates.getDayOfWeek(Dates.createDate(2014, Month.JANUARY, 1)));
        assertEquals(DayOfWeek.THURSDAY, Dates.getDayOfWeek(Dates.createDate(2014, Month.JANUARY, 2)));
        assertEquals(DayOfWeek.FRIDAY, Dates.getDayOfWeek(Dates.createDate(2014, Month.JANUARY, 3)));
        assertEquals(DayOfWeek.SATURDAY, Dates.getDayOfWeek(Dates.createDate(2014, Month.JANUARY, 4)));
        assertEquals(DayOfWeek.SUNDAY, Dates.getDayOfWeek(Dates.createDate(2014, Month.JANUARY, 5)));
        assertEquals(DayOfWeek.MONDAY, Dates.getDayOfWeek(Dates.createDate(2014, Month.JANUARY, 6)));
        assertEquals(DayOfWeek.TUESDAY, Dates.getDayOfWeek(Dates.createDate(2014, Month.JANUARY, 7)));

        assertEquals(DayOfWeek.SUNDAY, Dates.getDayOfWeek(Dates.createDate(2016, Month.FEBRUARY, 28)));
        assertEquals(DayOfWeek.MONDAY, Dates.getDayOfWeek(Dates.createDate(2016, Month.FEBRUARY, 29)));
        assertEquals(DayOfWeek.TUESDAY, Dates.getDayOfWeek(Dates.createDate(2016, Month.MARCH, 1)));
        assertEquals(DayOfWeek.WEDNESDAY, Dates.getDayOfWeek(Dates.createDate(2016, Month.MARCH, 2)));
        assertEquals(DayOfWeek.THURSDAY, Dates.getDayOfWeek(Dates.createDate(2016, Month.MARCH, 3)));
        assertEquals(DayOfWeek.FRIDAY, Dates.getDayOfWeek(Dates.createDate(2016, Month.MARCH, 4)));
        assertEquals(DayOfWeek.SATURDAY, Dates.getDayOfWeek(Dates.createDate(2016, Month.MARCH, 5)));
    }

    /**
     * Tests truncating function.
     */
    @Test
    public void testTrunc() {
        assertEquals(Dates.createTime(2019, Month.MARCH, 21, 14, 40, 19, 0), Dates.truncToSecond(Dates.createTime(2019, Month.MARCH, 21, 14, 40, 19, 321)));
        assertEquals(Dates.createTime(2019, Month.MARCH, 21, 14, 40, 0, 0), Dates.truncToMinute(Dates.createTime(2019, Month.MARCH, 21, 14, 40, 19, 321)));
        assertEquals(Dates.createTime(2019, Month.MARCH, 21, 14, 0, 0, 0), Dates.truncToHour(Dates.createTime(2019, Month.MARCH, 21, 14, 40, 19, 321)));
        assertEquals(Dates.createTime(2019, Month.MARCH, 21, 0, 0, 0, 0), Dates.truncToDay(Dates.createTime(2019, Month.MARCH, 21, 14, 40, 19, 321)));
        assertEquals(Dates.createTime(2019, Month.MARCH, 1, 0, 0, 0, 0), Dates.truncToMonth(Dates.createTime(2019, Month.MARCH, 21, 14, 40, 19, 321)));
        assertEquals(Dates.createTime(2019, Month.JANUARY, 1, 0, 0, 0, 0), Dates.truncToYear(Dates.createTime(2019, Month.MARCH, 21, 14, 40, 19, 321)));
    }

    /**
     * Tests the day difference.
     */
    @Test
    public void testGetDayDiff() {
        Date first = null;
        Date second = null;

        first = Dates.createTime(2012, Month.JANUARY, 1, 0, 0, 0);
        second = Dates.createTime(2012, Month.JANUARY, 1, 0, 0, 0);
        assertEquals(0, Dates.getDayDiff(first, second));

        first = Dates.createTime(2012, Month.JANUARY, 1, 0, 0, 0);
        second = Dates.createTime(2012, Month.JANUARY, 1, 23, 59, 59);
        assertEquals(0, Dates.getDayDiff(first, second));

        first = Dates.createTime(2012, Month.JANUARY, 1, 23, 59, 59);
        second = Dates.createTime(2012, Month.JANUARY, 1, 0, 0, 0);
        assertEquals(0, Dates.getDayDiff(first, second));

        first = Dates.createTime(2012, Month.JANUARY, 1, 0, 0, 0);
        second = Dates.createTime(2012, Month.JANUARY, 2, 0, 0, 0);
        assertEquals(1, Dates.getDayDiff(first, second));

        first = Dates.createTime(2012, Month.JANUARY, 1, 23, 59, 59);
        second = Dates.createTime(2012, Month.JANUARY, 2, 0, 0, 0);
        assertEquals(1, Dates.getDayDiff(first, second));

        first = Dates.createTime(2012, Month.JANUARY, 1, 23, 59, 59);
        second = Dates.createTime(2012, Month.JANUARY, 2, 23, 59, 59);
        assertEquals(1, Dates.getDayDiff(first, second));

        first = Dates.createTime(2012, Month.JANUARY, 2, 0, 0, 0);
        second = Dates.createTime(2012, Month.JANUARY, 1, 23, 59, 59);
        assertEquals(-1, Dates.getDayDiff(first, second));

        first = Dates.createTime(2012, Month.JANUARY, 1, 0, 0, 0);
        second = Dates.createTime(2012, Month.FEBRUARY, 1, 0, 0, 0);
        assertEquals(31, Dates.getDayDiff(first, second));
    }

    /**
     * Tests date formatting.
     */
    @Test
    public void testFormat() {
        Date date = Dates.createDate(1978, Month.MARCH, 3);
        assertEquals("03/03/1978", Dates.format(date, "MM/dd/yyyy"));
    }

    /**
     * Tests date paring.
     */
    @Test
    public void testParse() {
        assertEquals(Dates.createDate(1978, Month.MAY, 3), Dates.parse("05/03/1978", "MM/dd/yyyy"));
        assertEquals(Dates.createDate(1978, Month.MAY, 3), Dates.parse("05/03/1978", "MM/dd/yyyy HH:mm:ss", "MM/dd/yyyy"));
        assertEquals(Dates.createTime(1978, Month.MAY, 3, 14, 31, 5),
                Dates.parse("05/03/1978 14:31:05", "MM/dd/yyyy HH:mm:ss", "MM/dd/yyyy"));
        try {
            Dates.parse("03/03/1978");
        } catch (RuntimeException e) {
            assertEquals("no pattern matches the date format: date = 03/03/1978, patterns = []", e.getMessage());
        }
        try {
            Dates.parse("1978/05/03", "MM/dd/yyyy");
        } catch (RuntimeException e) {
            assertEquals("no pattern matches the date format: date = 1978/05/03, patterns = [MM/dd/yyyy]", e.getMessage());
        }
    }

}
