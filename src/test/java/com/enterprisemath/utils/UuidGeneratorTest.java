package com.enterprisemath.utils;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * Test case which proves the id generator.
 *
 * @author radek.hecl
 */
public class UuidGeneratorTest {

    /**
     * Creates new instance.
     */
    public UuidGeneratorTest() {
    }

    /**
     * Tests id generation.
     */
    @Test
    public void testGenerateId() {
        IdGenerator generator = UuidGenerator.create();
        String res = null;

        res = generator.generateId();
        assertTrue(res, res.matches("^[0-9a-f]{8}-[0-9abcdef]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"));
    }

}
