package com.enterprisemath.utils.cache;

import java.io.IOException;
import java.util.NoSuchElementException;
import org.apache.commons.lang3.builder.ToStringBuilder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

/**
 * Test case which proves in memory object cache.
 *
 * @author radek.hecl
 */
public class InMemoryObjectCacheTest {

    /**
     * Tested cache.
     */
    private ObjectCache cache;

    /**
     * Creates new instance.
     */
    public InMemoryObjectCacheTest() {
    }

    /**
     * Sets up the test environment.
     *
     * @throws IOException if there is an error with files
     */
    @Before
    public void setUp() throws IOException {
        cache = InMemoryObjectCache.create();
    }

    /**
     * Tests put method.
     *
     * @throws IOException if there is an error with reading the created files
     */
    @Test
    public void testPut() throws IOException {
        String str1 = "string1";
        String str2 = "string2";

        cache.put("key1", str1);
        assertEquals(str1, cache.get(String.class, "key1"));
        cache.put("key1", str2);
        assertEquals(str2, cache.get(String.class, "key1"));
    }

    /**
     * Tests remove method.
     */
    @Test
    public void testRemove() {
        String str1 = "string1";
        String str2 = "string2";

        cache.put("key1", str1);
        cache.put("key2", str2);

        cache.remove("key1");
        assertFalse(cache.contains("key1"));
        assertEquals(str2, cache.get(String.class, "key2"));
    }

    /**
     * Tests returning object.
     */
    @Test
    public void testGet() {
        String str1 = "string1";
        String str2 = "string2";

        cache.put("key1", str1);
        cache.put("key2", str2);

        assertEquals(str1, cache.get(String.class, "key1"));
        assertEquals(str2, cache.get(String.class, "key2"));

        try {
            cache.get(String.class, "unknown");
            fail("exception expected");
        } catch (NoSuchElementException e) {
            // expected
        }

        assertEquals(str1, cache.get(String.class, "key1", "not_found"));
        assertEquals(str2, cache.get(String.class, "key2", "not_found"));
        assertEquals("not_found", cache.get(String.class, "unknown", "not_found"));
    }

    /**
     * Tests clear method.
     */
    @Test
    public void testClear() {
        String str1 = "string1";
        String str2 = "string2";

        cache.put("key1", str1);
        cache.put("key2", str2);
        cache.clear();

        assertFalse(cache.contains("key1"));
        assertFalse(cache.contains("key2"));
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
