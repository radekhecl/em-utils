package com.enterprisemath.utils.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Implementation of object cache which stores everything in memory.
 *
 * @author radek.hecl
 */
public class InMemoryObjectCache implements ObjectCache {

    /**
     * Buffer map.
     */
    private Map<String, Object> buffer = new HashMap<>();

    /**
     * Object for locking.
     */
    private final Object lock = new Object();

    /**
     * Creates new instance.
     */
    private InMemoryObjectCache() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
    }

    @Override
    public void put(String key, Object object) {
        synchronized (lock) {
            buffer.put(key, object);
        }
    }

    @Override
    public void remove(String key) {
        synchronized (lock) {
            buffer.remove(key);
        }
    }

    @Override
    public <T> T get(Class<T> clazz, String key) {
        synchronized (lock) {
            if (!buffer.containsKey(key)) {
                throw new NoSuchElementException("element doesn't exists: key = " + key);
            }
            return (T) buffer.get(key);
        }
    }

    @Override
    public <T> T get(Class<T> clazz, String key, T def) {
        synchronized (lock) {
            if (!buffer.containsKey(key)) {
                return def;
            }
            return (T) buffer.get(key);
        }
    }

    @Override
    public boolean contains(String key) {
        synchronized (lock) {
            return buffer.containsKey(key);
        }
    }

    @Override
    public void clear() {
        synchronized (lock) {
            buffer.clear();
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @return created instance
     */
    public static InMemoryObjectCache create() {
        InMemoryObjectCache res = new InMemoryObjectCache();
        res.guardInvariants();
        return res;
    }
}
