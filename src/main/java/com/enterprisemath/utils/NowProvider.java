package com.enterprisemath.utils;

import java.time.Instant;
import java.util.Date;

/**
 * Functionality for serving the current timestamp related objects.
 *
 * @author radek.hecl
 */
public interface NowProvider {

    /**
     * Returns current timestamp as the date object.
     *
     * @return current timestamp as the date object
     */
    public Date date();

    /**
     * Returns current timestamp as the instant object.
     *
     * @return current timestamp as the instant object
     */
    public Instant instant();

}
