package com.enterprisemath.utils;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Test case for CSV utilities.
 * Data example is taken from Wikipedia (Jan 2016).
 *
 * @author radek.hecl
 */
public class CsvTest {

    /**
     * Creates new instance.
     */
    public CsvTest() {
    }

    /**
     * Tests parsing.
     */
    @Test
    public void testParse() {
        String data = null;
        List<List<String>> expected = null;
        List<List<String>> res = null;

        data = "Year,Make,Model,Description,Price\n" +
                "1997,Ford,E350,\"ac, abs, moon\",3000.00\n" +
                "\"1999\",Chevy,\"Venture \"\"Extended Edition\"\"\",\"\",\"4900.00\"\n" +
                "1999,Chevy,\"Venture \"\"Extended Edition, Very Large\"\"\",,5000.00\n" +
                "1996,Jeep,Grand Cherokee,\"MUST SELL!\n" +
                "air, moon roof, loaded\",4799.00\n";
        expected = new ArrayList<>();
        expected.add(Arrays.asList("Year", "Make", "Model", "Description", "Price"));
        expected.add(Arrays.asList("1997", "Ford", "E350", "ac, abs, moon", "3000.00"));
        expected.add(Arrays.asList("1999", "Chevy", "Venture \"Extended Edition\"", "", "4900.00"));
        expected.add(Arrays.asList("1999", "Chevy", "Venture \"Extended Edition, Very Large\"", "", "5000.00"));
        expected.add(Arrays.asList("1996", "Jeep", "Grand Cherokee", "MUST SELL!\nair, moon roof, loaded", "4799.00"));
        res = Csv.parse(data.getBytes(Charset.forName("utf-8")));
        assertEquals(expected, res);
    }

    /**
     * Tests formatting.
     */
    @Test
    public void testFormat() {
        List<List<String>> data = null;
        String expected = null;
        byte[] res = null;

        data = new ArrayList<>();
        data.add(Arrays.asList("Year", "Make", "Model", "Description", "Price"));
        data.add(Arrays.asList("1997", "Ford", "E350", "ac, abs, moon", "3000.00"));
        data.add(Arrays.asList("1999", "Chevy", "Venture \"Extended Edition\"", "", "4900.00"));
        data.add(Arrays.asList("1999", "Chevy", "Venture \"Extended Edition, Very Large\"", null, "5000.00"));
        data.add(Arrays.asList("1996", "Jeep", "Grand Cherokee", "MUST SELL!\nair, moon roof, loaded", "4799.00"));
        expected = "\"Year\",\"Make\",\"Model\",\"Description\",\"Price\"\n" +
                "\"1997\",\"Ford\",\"E350\",\"ac, abs, moon\",\"3000.00\"\n" +
                "\"1999\",\"Chevy\",\"Venture \"\"Extended Edition\"\"\",\"\",\"4900.00\"\n" +
                "\"1999\",\"Chevy\",\"Venture \"\"Extended Edition, Very Large\"\"\",\"\",\"5000.00\"\n" +
                "\"1996\",\"Jeep\",\"Grand Cherokee\",\"MUST SELL!\n" +
                "air, moon roof, loaded\",\"4799.00\"\n";
        res = Csv.format(data);
        Assert.assertEquals(expected, new String(res, Charset.forName("utf-8")));
    }

}
