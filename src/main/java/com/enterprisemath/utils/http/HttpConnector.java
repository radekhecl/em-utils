package com.enterprisemath.utils.http;

/**
 * Defines functionality for connecting to the http server.
 * This interface provides simple connectivity methods.
 * All methods returns only if response is successful.
 * In other case exception is thrown.
 *
 * @author radek.hecl
 *
 */
public interface HttpConnector {

    /**
     * Executes request and returns string response.
     * This method returns normally only if the response code is success (2XX).
     *
     * @param request request
     * @return response string
     * @throws RuntimeException if response code is anything else than success (2XX)
     */
    public String executeString(HttpReq request);

    /**
     * Executes request and returns raw byte array response.
     * This method returns normally only if the response code is success (2XX).
     *
     * @param request request
     * @return response bytes
     * @throws RuntimeException if response code is anything else than success (2XX)
     */
    public byte[] executeBytes(HttpReq request);

    /**
     * Executes request and returns full response. This method returns response for every response code.
     * It is up to the caller to verify that response was successful or not. This allows detail translation of error codes.
     *
     * @param request request
     * @return response
     */
    public HttpResp execute(HttpReq request);
}
