package com.enterprisemath.utils;

import java.util.ArrayList;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;

/**
 * Test case to prove validation utility method are working.
 *
 * @author radek.hecl
 */
public class GuardTest {

    /**
     * Creates new instance.
     */
    public GuardTest() {
    }

    /**
     * Tests guarding true method.
     */
    @Test
    public void testBeTrue() {
        // non formatted
        Guard.beTrue(true, "test");
        Guard.beTrue(true, "test %s", "argument");
        try {
            Guard.beTrue(false, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.beTrue(false, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
    }

    /**
     * Tests guarding false method.
     */
    @Test
    public void testBeFalse() {
        Guard.beFalse(false, "test");
        Guard.beFalse(false, "test %s", "argument");
        try {
            Guard.beFalse(true, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.beFalse(true, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
    }

    /**
     * Tests guarding objects to be null.
     */
    @Test
    public void testBeNull() {
        Guard.beNull(null, "test");
        Guard.beNull(null, "test %s", "argument");
        try {
            Guard.beNull(new Object(), "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.beNull(new Object(), "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
    }

    /**
     * Tests guarding objects to be not null.
     */
    @Test
    public void testNotNull() {
        Guard.notNull(new Object(), "test");
        Guard.notNull(new Object(), "test %s", "argument");
        try {
            Guard.notNull(null, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notNull(null, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
    }

    /**
     * Test to prove guarding 2 objects to be equal.
     */
    @Test
    public void testEquals() {
        Guard.equals(null, null, "test");
        Guard.equals(null, null, "test %", "argument");
        Guard.equals("hello", "hello", "test");
        Guard.equals("hello", "hello", "test %s", "argument");

        //
        try {
            Guard.equals("one", "two", "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.equals("one", "two", "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        //
        try {
            Guard.equals("one", null, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.equals("one", null, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        //
        try {
            Guard.equals(null, "two", "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.equals(null, "two", "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
    }

    /**
     * Tests guarding number to be positive.
     */
    @Test
    public void testPositive() {
        // byte
        Guard.positive((byte) 1, "test");
        Guard.positive((byte) 1, "test %s", "argument");
        try {
            Guard.positive((byte) 0, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.positive((byte) 0, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        // short
        Guard.positive((short) 1, "test");
        Guard.positive((short) 1, "test %s", "argument");
        try {
            Guard.positive((short) 0, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.positive((short) 0, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        // int
        Guard.positive(1, "test");
        Guard.positive(1, "test %s", "argument");
        try {
            Guard.positive(0, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.positive(0, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        // long
        Guard.positive(1L, "test");
        Guard.positive(1L, "test %s", "argument");
        try {
            Guard.positive(0L, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.positive(0L, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        // float
        Guard.positive(0.001f, "test");
        Guard.positive(0.001f, "test %s", "argument");
        Guard.positive(Float.POSITIVE_INFINITY, "test");
        Guard.positive(Float.POSITIVE_INFINITY, "test %s", "argument");
        try {
            Guard.positive(0f, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.positive(0f, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
        try {
            Guard.positive(Float.NaN, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.positive(Float.NaN, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
        try {
            Guard.positive(Float.NEGATIVE_INFINITY, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.positive(Float.NEGATIVE_INFINITY, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        // double
        Guard.positive(0.001, "test");
        Guard.positive(0.001, "test %s", "argument");
        Guard.positive(Double.POSITIVE_INFINITY, "test");
        Guard.positive(Double.POSITIVE_INFINITY, "test %s", "argument");
        try {
            Guard.positive(0.0, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.positive(0.0, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
        try {
            Guard.positive(Double.NaN, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.positive(Double.NaN, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
        try {
            Guard.positive(Double.NEGATIVE_INFINITY, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.positive(Double.NEGATIVE_INFINITY, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
    }

    /**
     * Tests guarding number to be positive.
     */
    @Test
    public void testNotNegative() {
        // byte
        Guard.notNegative((byte) 0, "test");
        Guard.notNegative((byte) 0, "test %s", "argument");
        Guard.notNegative((byte) 1, "test");
        Guard.notNegative((byte) 1, "test %s", "argument");
        try {
            Guard.notNegative((byte) -1, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notNegative((byte) -1, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        // short
        Guard.notNegative((short) 0, "test");
        Guard.notNegative((short) 0, "test %s", "argument");
        Guard.notNegative((short) 1, "test");
        Guard.notNegative((short) 1, "test %s", "argument");
        try {
            Guard.notNegative((short) -1, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notNegative((short) -1, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        // int
        Guard.notNegative(0, "test");
        Guard.notNegative(0, "test %s", "argument");
        Guard.notNegative(1, "test");
        Guard.notNegative(1, "test %s", "argument");
        try {
            Guard.notNegative(-1, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notNegative(-1, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        // long
        Guard.notNegative(0L, "test");
        Guard.notNegative(0L, "test %s", "argument");
        Guard.notNegative(1L, "test");
        Guard.notNegative(1L, "test %s", "argument");
        try {
            Guard.notNegative(-1L, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notNegative(-1L, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        // float
        Guard.notNegative(0.0f, "test");
        Guard.notNegative(0.0f, "test %s", "argument");
        Guard.notNegative(0.001f, "test");
        Guard.notNegative(0.001f, "test %s", "argument");
        Guard.notNegative(Float.POSITIVE_INFINITY, "test");
        Guard.notNegative(Float.POSITIVE_INFINITY, "test %s", "argument");
        try {
            Guard.notNegative(-0.001f, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notNegative(-0.001f, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
        try {
            Guard.notNegative(Float.NaN, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notNegative(Float.NaN, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
        try {
            Guard.notNegative(Float.NEGATIVE_INFINITY, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notNegative(Float.NEGATIVE_INFINITY, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        // double
        Guard.notNegative(0.0, "test");
        Guard.notNegative(0.0, "test %s", "argument");
        Guard.notNegative(0.001, "test");
        Guard.notNegative(0.001, "test %s", "argument");
        Guard.notNegative(Double.POSITIVE_INFINITY, "test");
        Guard.notNegative(Double.POSITIVE_INFINITY, "test %s", "argument");
        try {
            Guard.notNegative(-0.001, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notNegative(-0.001, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
        try {
            Guard.notNegative(Double.NaN, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notNegative(Double.NaN, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
        try {
            Guard.notNegative(Double.NEGATIVE_INFINITY, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notNegative(Double.NEGATIVE_INFINITY, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
    }

    /**
     * Test to prove guard empty string.
     */
    @Test
    public void testEmpty() {
        Guard.empty(null, "test");
        Guard.empty(null, "test %", "argument");
        Guard.empty("", "test");
        Guard.empty("", "test %", "argument");

        //
        try {
            Guard.empty("a", "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.empty("a", "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
    }

    /**
     * Test to prove the guarding of empty strings.
     */
    @Test
    public void testNotEmpty() {
        Guard.notEmpty("hello", "test");
        Guard.notEmpty("hello", "test %s", "argument");

        try {
            Guard.notEmpty(null, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notEmpty(null, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        try {
            Guard.notEmpty("", "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notEmpty("", "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
    }

    /**
     * Test to prove guarding the specified pattern of string.
     */
    @Test
    public void testMatch() {
        Guard.match("12345", "^[0-9]+$", "test");
        Guard.match("12345", "^[0-9]+$", "test %s", "argument");

        //
        try {
            Guard.match(null, "^[0-9]+$", "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.match(null, "^[0-9]+$", "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        //
        try {
            Guard.match("abc", "^[0-9]+$", "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.match("abc", "^[0-9]+$", "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
    }

    /**
     * Test to prove guarding object to be within the specified collection.
     */
    @Test
    public void testIn() {
        Guard.in(1, Arrays.asList(1, 2), "test");
        Guard.in(1, Arrays.asList(1, 2), "test %", "argument");
        Guard.in(null, Arrays.asList(1, null), "test");
        Guard.in(null, Arrays.asList(1, null), "test %", "argument");

        //
        try {
            Guard.in(0, Arrays.asList(1, 2), "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.in(0, Arrays.asList(1, 2), "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
    }

    /**
     * Test to prove that collection does not have a null element.
     */
    @Test
    public void testNotNullCollection() {
        Guard.notNullCollection(Arrays.asList("str1", "str2"), "test");
        Guard.notNullCollection(Arrays.asList("str1", "str2"), "test %s", "argument");

        //
        try {
            Guard.notNullCollection(null, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notNullCollection(null, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        //
        try {
            Guard.notNullCollection(Arrays.asList("str1", null), "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notNullCollection(Arrays.asList("str1", null), "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
    }

    /**
     * Test to prove guarding collection to having an empty string.
     */
    @Test
    public void testnotEmptyStringCollection() {
        Guard.notEmptyStringCollection(new ArrayList<>(), "test");
        Guard.notEmptyStringCollection(new ArrayList<>(), "test %s", "argument");
        Guard.notEmptyStringCollection(Arrays.asList("str1", "str2"), "test %s", "argument");

        //
        try {
            Guard.notEmptyStringCollection(null, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notEmptyStringCollection(null, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        //
        try {
            Guard.notEmptyStringCollection(Arrays.asList("str", null), "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notEmptyStringCollection(Arrays.asList("str", null), "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        //
        try {
            Guard.notEmptyStringCollection(Arrays.asList("str", ""), "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notEmptyStringCollection(Arrays.asList("str", ""), "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
    }

    /**
     * Test to prove map does not have null key or value.
     */
    @Test
    public void testNotNullMap() {
        Guard.notNullMap(Dut.map("one", 1), "test");
        Guard.notNullMap(Dut.map("one", 1), "test %s", "argument");
        Guard.notNullMap(Dut.map("", 1), "test");
        Guard.notNullMap(Dut.map("", 1), "test %s", "argument");

        //
        try {
            Guard.notNullMap(null, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notNullMap(null, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        //
        try {
            Guard.notNullMap(Dut.map(null, 1), "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
        try {
            Guard.notNullMap(Dut.map(null, 1), "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }

        //
        try {
            Guard.notNullMap(Dut.map("one", null), "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        try {
            Guard.notNullMap(Dut.map("one", null), "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }

    }

    /**
     * Test to prove map does not have empty key or null value.
     */
    @Test
    public void testNotEmptyNullMap() {
        Guard.notEmptyNullMap(Dut.map("one", 1), "test");
        Guard.notEmptyNullMap(Dut.map("one", 1), "test %s", "argument");

        //
        try {
            Guard.notEmptyNullMap(null, "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }
        try {
            Guard.notEmptyNullMap(null, "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        //
        try {
            Guard.notEmptyNullMap(Dut.map(null, 1), "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }
        try {
            Guard.notEmptyNullMap(Dut.map(null, 1), "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }

        //
        try {
            Guard.notEmptyNullMap(Dut.map("", 1), "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        try {
            Guard.notEmptyNullMap(Dut.map("", 1), "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }

        //
        try {
            Guard.notEmptyNullMap(Dut.map("one", null), "test %s", "argument");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test argument", e.getMessage());
        }

        try {
            Guard.notEmptyNullMap(Dut.map("one", null), "test");
            fail("exception expected");
        } catch (IllegalArgumentException e) {
            assertEquals("test", e.getMessage());
        }

    }

}
