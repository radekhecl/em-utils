# EM Utils

EM-utils is a collection of high-level reusable methods and components.
Main motivations are following.

- Code robustness
- Unit testing
- Parallel Processing

## Dependency Details

**Current stable release: ** 4.1.1

The project uses a Maven build system and binaries are deployed in The Central Repository.
Dependency coordinates are the following.
```
<dependency>
    <groupId>com.enterprisemath</groupId>
    <artifactId>em-utils</artifactId>
    <version>4.1.1</version>
</dependency>
```

## Licence

The project is licensed under MIT license.
Simply said, do whatever you want and don't blame me if anything goes wrong.
If you use this library and you find it useful, please drop me a message.

# Features

- Object validation methods are inside `Guard` class.
- Basic operations for safe objects are inside `Dut` class.
- Arbitrary precision numbers are covered by `ANumber` class.
- `NowProvider` defines functionality to get current timestamp. `DefaultNowProvider` is for applications and `ConstantNowProvider` as a mock for unit tests.
- `IdGenerator` defines functionality to generate unique ids, like database primary keys. `UuidGenerator` is for applications and `SequenceIdGenerator` can be used in the unit tests.
- `HttpConnector` defines very simple HTTP request-response client. Main implementation is `DefaultHttpConnector`.
- Caching functionality is defined by `ObjectCache`. Available implementations are `InMemoryObjectCache` and `ExpiringInMemoryObjectCache`.
- Others are `Json`, `Csv`, `StringParser` and `Dates`.
