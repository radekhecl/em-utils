package com.enterprisemath.utils;

import java.time.Instant;
import java.util.Date;

/**
 * Now provider returning always the same value.
 *
 * @author radek.hecl
 */
public class ConstantNowProvider implements NowProvider {

    /**
     * Instant considered as now.
     */
    private Instant now;

    /**
     * Creates new instance.
     */
    public ConstantNowProvider() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        Guard.notNull(now, "now cannot be null");
    }

    @Override
    public Date date() {
        return Date.from(now);
    }

    @Override
    public Instant instant() {
        return now;
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new instance with the instant as a now point.
     * Returned instant will be always the same as the given one.
     * Returned date will be shifted according to the computer time zone.
     *
     * @param now instant used as now point
     * @return created provider
     */
    public static ConstantNowProvider create(Instant now) {
        ConstantNowProvider res = new ConstantNowProvider();
        res.now = now;
        res.guardInvariants();
        return res;
    }

    /**
     * Creates new instance with the date as a now point.
     * Returned instant will be shifted according to the computer time zone.
     * Returned date will be always the same as the given one.
     *
     * @param now date used as a now point
     * @return created provider
     */
    public static ConstantNowProvider create(Date now) {
        ConstantNowProvider res = new ConstantNowProvider();
        res.now = now.toInstant();
        res.guardInvariants();
        return res;
    }

}
