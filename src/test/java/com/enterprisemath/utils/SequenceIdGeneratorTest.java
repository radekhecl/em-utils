package com.enterprisemath.utils;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Test case which proves the id generator.
 *
 * @author radek.hecl
 */
public class SequenceIdGeneratorTest {

    /**
     * Creates new instance.
     */
    public SequenceIdGeneratorTest() {
    }

    /**
     * Tests id generation.
     */
    @Test
    public void testGenerateId() {
        IdGenerator generator = null;

        generator = SequenceIdGenerator.create();
        assertEquals("1", generator.generateId());
        assertEquals("2", generator.generateId());
        assertEquals("3", generator.generateId());

        generator = SequenceIdGenerator.create("C-");
        assertEquals("C-1", generator.generateId());
        assertEquals("C-2", generator.generateId());
        assertEquals("C-3", generator.generateId());
    }

}
