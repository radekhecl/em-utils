package com.enterprisemath.utils;

/**
 * Pair of the arbitrary number and remainder.
 * This is the result after division operation.
 *
 * @author radek.hecl
 */
public final class ANumberRemainderPair {

    /**
     * Number.
     */
    private ANumber number;

    /**
     * Remainder.
     */
    private ANumber remainder;

    /**
     * Creates new instance.
     */
    private ANumberRemainderPair() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        Guard.notNull(number, "number cannot be null");
        Guard.notNull(remainder, "remainder cannot be null");
    }

    /**
     * Returns number.
     *
     * @return number
     */
    public ANumber getNumber() {
        return number;
    }

    /**
     * Returns remainder.
     *
     * @return remainder
     */
    public ANumber getRemainder() {
        return remainder;
    }

    @Override
    public int hashCode() {
        return Dut.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return Dut.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param number number
     * @param remainder remainder
     * @return number remainder pair
     */
    public static ANumberRemainderPair create(ANumber number, ANumber remainder) {
        ANumberRemainderPair res = new ANumberRemainderPair();
        res.number = number;
        res.remainder = remainder;
        res.guardInvariants();
        return res;
    }

}
