package com.enterprisemath.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Collections of static method for working with domain objects.
 *
 * @author radek.hecl
 */
public class Dut {

    /**
     * Prevents construction.
     */
    private Dut() {
    }

    /**
     * Creates list from the given elements.
     *
     * @param <T> object type
     * @param objs objects
     * @return created list
     */
    public static <T> List<T> list(T... objs) {
        return Arrays.asList(objs);
    }

    /**
     * Creates immutable list from the given elements.
     *
     * @param <T> object type
     * @param objs objects
     * @return created list
     */
    public static <T> List<T> immutableList(T... objs) {
        return Collections.unmodifiableList(list(objs));
    }

    /**
     * Copies given collection into a list in a null safe way.
     * Returns empty list if source is null. Only shallow copy is performed.
     *
     * @param <T> object type
     * @param source source collection
     * @return copied list
     */
    public static <T> List<T> copyList(Collection<T> source) {
        if (source == null) {
            source = new ArrayList<>();
        }
        return new ArrayList<>(source);
    }

    /**
     * Copies given collection into an immutable list in a null safe way.
     * Returns empty list if source is null. Only shallow copy is performed.
     *
     * @param <T> object type
     * @param source source collection
     * @return copied list
     */
    public static <T> List<T> copyImmutableList(Collection<T> source) {
        return Collections.unmodifiableList(copyList(source));
    }

    /**
     * Creates set from the given elements.
     *
     * @param <T> object type
     * @param objs objects
     * @return created set
     */
    public static <T> Set<T> set(T... objs) {
        return new HashSet<>(Arrays.asList(objs));
    }

    /**
     * Creates immutable set from the given elements.
     *
     * @param <T> object type
     * @param objs objects
     * @return created set
     */
    public static <T> Set<T> immutableSet(T... objs) {
        return Collections.unmodifiableSet(set(objs));
    }

    /**
     * Copies given collection into a set in a null safe way.
     * Returns empty set if source is null. Only shallow copy is performed.
     *
     * @param <T> object type
     * @param source source collection
     * @return copied set
     */
    public static <T> Set<T> copySet(Collection<T> source) {
        if (source == null) {
            source = new HashSet<>();
        }
        return new HashSet<>(source);
    }

    /**
     * Copies given collection into an immutable set in a null safe way.
     * Returns empty set if source is null. Only shallow copy is performed.
     *
     * @param <T> object type
     * @param source source collection
     * @return copied set
     */
    public static <T> Set<T> copyImmutableSet(Collection<T> source) {
        return Collections.unmodifiableSet(copySet(source));
    }

    /**
     * Creates sorted set from the given elements.
     *
     * @param <T> object type
     * @param objs objects
     * @return created sorted set
     */
    public static <T extends Comparable<?>> SortedSet<T> sortedSet(T... objs) {
        return new TreeSet<>(Arrays.asList(objs));
    }

    /**
     * Creates sorted set from the given elements.
     *
     * @param <T> object type
     * @param comparator comparator
     * @param objs objects
     * @return created sorted set
     */
    public static <T> SortedSet<T> sortedSet(Comparator<T> comparator, T... objs) {
        SortedSet<T> res = new TreeSet<>(comparator);
        res.addAll(Arrays.asList(objs));
        return res;
    }

    /**
     * Creates immutable sorted set from the given elements.
     *
     * @param <T> object type
     * @param objs objects
     * @return created sorted set
     */
    public static <T extends Comparable<?>> SortedSet<T> immutableSortedSet(T... objs) {
        return Collections.unmodifiableSortedSet(sortedSet(objs));
    }

    /**
     * Creates immutable sorted set from the given elements.
     *
     * @param <T> object type
     * @param comparator comparator
     * @param objs objects
     * @return created sorted set
     */
    public static <T> SortedSet<T> immutableSortedSet(Comparator<T> comparator, T... objs) {
        return Collections.unmodifiableSortedSet(sortedSet(comparator, objs));
    }

    /**
     * Copies given collection into a sorted set in a null safe way.
     * Returns empty sorted set if source is null. Only shallow copy is performed.
     *
     * @param <T> object type
     * @param source source collection
     * @return copied sorted set
     */
    public static <T extends Comparable<?>> SortedSet<T> copySortedSet(Collection<T> source) {
        if (source == null) {
            source = new TreeSet<>();
        }
        return new TreeSet<>(source);
    }

    /**
     * Copies given collection into a sorted set in a null safe way.
     * Returns empty sorted set if source is null. Only shallow copy is performed.
     *
     * @param <T> object type
     * @param comparator comparator
     * @param source source collection
     * @return copied sorted set
     */
    public static <T> SortedSet<T> copySortedSet(Comparator<T> comparator, Collection<T> source) {
        if (source == null) {
            source = new TreeSet<>(comparator);
        }
        SortedSet<T> res = new TreeSet<>(comparator);
        res.addAll(source);
        return res;
    }

    /**
     * Copies given collection into an immutable sorted set in a null safe way.
     * Returns empty sorted set if source is null. Only shallow copy is performed.
     *
     * @param <T> object type
     * @param source source collection
     * @return copied sorted set
     */
    public static <T extends Comparable<?>> SortedSet<T> copyImmutableSortedSet(Collection<T> source) {
        return Collections.unmodifiableSortedSet(copySortedSet(source));
    }

    /**
     * Copies given collection into an immutable sorted set in a null safe way.
     * Returns empty sorted set if source is null. Only shallow copy is performed.
     *
     * @param <T> object type
     * @param comparator comparator
     * @param source source collection
     * @return copied sorted set
     */
    public static <T> SortedSet<T> copyImmutableSortedSet(Comparator<T> comparator, Collection<T> source) {
        return Collections.unmodifiableSortedSet(copySortedSet(comparator, source));
    }

    /**
     * Creates map from the given elements.
     *
     * @param <K> key type
     * @param <V> value type
     * @param key first key
     * @param val first value
     * @param data array with other keys and values, array goes [key1, value1, key2, value2...] and it is responsibility
     * of the caller to take care about class types
     * @return created map
     */
    public static <K, V> Map<K, V> map(K key, V val, Object... data) {
        Map<K, V> res = new HashMap<>();
        res.put(key, val);
        for (int i = 0; i < data.length; i = i + 2) {
            res.put((K) data[i], (V) data[i + 1]);
        }
        return res;
    }

    /**
     * Creates immutable map from the given elements.
     *
     * @param <K> key type
     * @param <V> value type
     * @param key first key
     * @param val first value
     * @param data array with other keys and values, array goes [key1, value1, key2, value2...] and it is responsibility
     * of the caller to take care about class types
     * @return created map
     */
    public static <K, V> Map<K, V> immutableMap(K key, V val, Object... data) {
        return Collections.unmodifiableMap(map(key, val, data));
    }

    /**
     * Copies given map in a null safe way.
     * Returns empty map if source is null. Only shallow copy is performed.
     *
     * @param <K> key type
     * @param <V> value type
     * @param source source map
     * @return copied map
     */
    public static <K, V> Map<K, V> copyMap(Map<K, V> source) {
        if (source == null) {
            source = new HashMap<>();
        }
        return new HashMap<>(source);
    }

    /**
     * Copies given map into an immutable version in a null safe way.
     * Returns empty map if source is null. Only shallow copy is performed.
     *
     * @param <K> key type
     * @param <V> value type
     * @param source source map
     * @return copied map
     */
    public static <K, V> Map<K, V> copyImmutableMap(Map<K, V> source) {
        return Collections.unmodifiableMap(copyMap(source));
    }

    /**
     * Creates sorted map from the given elements.
     *
     * @param <K> key type
     * @param <V> value type
     * @param key first key
     * @param val first value
     * @param data array with other keys and values, array goes [key1, value1, key2, value2...] and it is responsibility
     * of the caller to take care about class types
     * @return created sorted map
     */
    public static <K extends Comparable<?>, V> SortedMap<K, V> sortedMap(K key, V val, Object... data) {
        SortedMap<K, V> res = new TreeMap<>();
        res.put(key, val);
        for (int i = 0; i < data.length; i = i + 2) {
            res.put((K) data[i], (V) data[i + 1]);
        }
        return res;
    }

    /**
     * Creates sorted map from the given elements.
     *
     * @param <K> key type
     * @param <V> value type
     * @param comparator comparator
     * @param key first key
     * @param val first value
     * @param data array with other keys and values, array goes [key1, value1, key2, value2...] and it is responsibility
     * of the caller to take care about class types
     * @return created sorted map
     */
    public static <K, V> SortedMap<K, V> sortedMap(Comparator<K> comparator, K key, V val, Object... data) {
        SortedMap<K, V> res = new TreeMap<>(comparator);
        res.put(key, val);
        for (int i = 0; i < data.length; i = i + 2) {
            res.put((K) data[i], (V) data[i + 1]);
        }
        return res;
    }

    /**
     * Creates immutable sorted map from the given elements.
     *
     * @param <K> key type
     * @param <V> value type
     * @param key first key
     * @param val first value
     * @param data array with other keys and values, array goes [key1, value1, key2, value2...] and it is responsibility
     * of the caller to take care about class types
     * @return created sorted map
     */
    public static <K extends Comparable<?>, V> SortedMap<K, V> immutableSortedMap(K key, V val, Object... data) {
        return Collections.unmodifiableSortedMap(sortedMap(key, val, data));
    }

    /**
     * Creates immutable sorted map from the given elements.
     *
     * @param <K> key type
     * @param <V> value type
     * @param comparator comparator
     * @param key first key
     * @param val first value
     * @param data array with other keys and values, array goes [key1, value1, key2, value2...] and it is responsibility
     * of the caller to take care about class types
     * @return created sorted map
     */
    public static <K, V> SortedMap<K, V> immutableSortedMap(Comparator<K> comparator, K key, V val, Object... data) {
        return Collections.unmodifiableSortedMap(sortedMap(comparator, key, val, data));
    }

    /**
     * Copies given map into a sorted map in a null safe way.
     * Returns empty map if source is null. Only shallow copy is performed.
     *
     * @param <K> key type
     * @param <V> value type
     * @param source source map
     * @return copied map
     */
    public static <K extends Comparable<?>, V> SortedMap<K, V> copySortedMap(Map<K, V> source) {
        if (source == null) {
            source = new TreeMap<>();
        }
        return new TreeMap<>(source);
    }

    /**
     * Copies given map into a sorted map in a null safe way.
     * Returns empty map if source is null. Only shallow copy is performed.
     *
     * @param <K> key type
     * @param <V> value type
     * @param comparator comparator
     * @param source source map
     * @return copied map
     */
    public static <K, V> SortedMap<K, V> copySortedMap(Comparator<K> comparator, Map<K, V> source) {
        if (source == null) {
            source = new TreeMap<>(comparator);
        }
        SortedMap<K, V> res = new TreeMap<>(comparator);
        res.putAll(source);
        return res;
    }

    /**
     * Copies given map into an immutable sorted map in a null safe way.
     * Returns empty map if source is null. Only shallow copy is performed.
     *
     * @param <K> key type
     * @param <V> value type
     * @param source source map
     * @return copied map
     */
    public static <K extends Comparable<?>, V> SortedMap<K, V> copyImmutableSortedMap(Map<K, V> source) {
        return Collections.unmodifiableSortedMap(copySortedMap(source));
    }

    /**
     * Copies given map into an immutable sorted map in a null safe way.
     * Returns empty map if source is null. Only shallow copy is performed.
     *
     * @param <K> key type
     * @param <V> value type
     * @param comparator comparator
     * @param source source map
     * @return copied map
     */
    public static <K, V> SortedMap<K, V> copyImmutableSortedMap(Comparator<K> comparator, Map<K, V> source) {
        return Collections.unmodifiableSortedMap(copySortedMap(comparator, source));
    }

    /**
     * Takes the collection and splits it into the several lists.
     * Every of these lists has maximum number of elements which is less or equal to the max parameters.
     * If source collection has some duplicated elements, then all of them stays there.
     * If source collection is null or empty, then list with no sub lists will be returned.
     * The ordering within the split lists is the same as within the source collection.
     *
     * @param <T> inner object type
     * @param source source collection
     * @param max maximum allowed number of elements per one list, must be positive number
     * @return source collection split into lists
     */
    public static <T> List<List<T>> splitToLists(Collection<T> source, int max) {
        Guard.positive(max, "max must be positive");
        if (source == null || source.isEmpty()) {
            return Collections.emptyList();
        }
        List<List<T>> res = new ArrayList<>();
        List<T> part = new ArrayList<>();
        for (T obj : source) {
            part.add(obj);
            if (part.size() == max) {
                res.add(part);
                part = new ArrayList<>();
            }
        }
        if (!part.isEmpty()) {
            res.add(part);
        }
        return res;
    }

    /**
     * Null safe copy date function.
     *
     * @param source source date, can be null
     * @return copy of the source date or null if source is null
     */
    public static Date copyDate(Date source) {
        if (source == null) {
            return null;
        }
        return new Date(source.getTime());
    }

    /**
     * Null safe equals method. Returns true if both objects are null or equals each other.
     * This method expects equals method to be well defined (reflexivity, symmetricity, transitivity) in both objects.
     *
     * @param obj1 first object
     * @param obj2 second object
     * @return true if both objects are null or if they equals, false otherwise
     */
    public static boolean safeEquals(Object obj1, Object obj2) {
        if (obj1 == null && obj2 == null) {
            return true;
        }
        if (obj1 == null || obj2 == null) {
            return false;
        }
        return obj1.equals(obj2);
    }

    /**
     * Creates reflection hash code of the object.
     * This method simply uses all the fields within the class.
     * This method doesn't check the cyclic reference.
     * This method uses reflection and makes the private fields accessible.
     *
     * @param obj object
     * @return hash code
     */
    public static int reflectionHashCode(Object obj) {
        return HashCodeBuilder.reflectionHashCode(obj);
    }

    /**
     * Returns whether 2 objects are deeply equals or not.
     * This method simply uses all the fields within the class.
     * This method doesn't check the cyclic reference.
     * This method uses reflection and makes the private fields accessible.
     *
     * @param obj1 first object
     * @param obj2 second object
     * @return true, if objects are deeply equals, false otherwise
     */
    public static boolean reflectionEquals(Object obj1, Object obj2) {
        return EqualsBuilder.reflectionEquals(obj1, obj2);
    }

    /**
     * Returns string representation of this object, dumping all fields.
     * This method uses reflection and makes the private fields accessible.
     *
     * @param obj object
     * @return string representation
     */
    public static String reflectionToString(Object obj) {
        return ToStringBuilder.reflectionToString(obj);
    }
}
