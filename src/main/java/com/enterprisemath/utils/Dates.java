package com.enterprisemath.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Month;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.time.DateUtils;

/**
 * Class with utility methods for work with dates and times.
 *
 * @author radek.hecl
 */
public class Dates {

    /**
     * Prevents construction from outside.
     */
    private Dates() {
    }

    /**
     * Returns the current timestamp.
     *
     * @return current timestamp
     */
    public static Date now() {
        return new Date();
    }

    /**
     * Returns todays date. This is current timestamp which is truncated to days.
     *
     * @return todays date
     */
    public static Date today() {
        return DateUtils.truncate(new Date(), Calendar.DATE);
    }

    /**
     * Returns the yesterdays date. This is truncated to date.
     *
     * @return yesterdays date
     */
    public static Date yesterday() {
        return DateUtils.addDays(today(), -1);
    }

    /**
     * Returns the tomorrows date. This is truncated to date.
     *
     * @return tomorrows date
     */
    public static Date tomorrow() {
        return DateUtils.addDays(today(), 1);
    }

    /**
     * Returns the date which is shifted for a particular number of days from
     * today. This is truncated to date.
     *
     * @param numDays the number of days to shift
     * @return date shifted from today
     */
    public static Date fromToday(int numDays) {
        return DateUtils.addDays(today(), numDays);
    }

    /**
     * Creates date according the Gregorian calendar input.
     *
     * @param year year
     * @param month month
     * @param day day of the month (1-31, range depends on the month and year)
     * @return created date
     */
    public static Date createDate(int year, Month month, int day) {
        return createTime(year, month, day, 0, 0, 0, 0);
    }

    /**
     * Creates time according the Gregorian calendar input.
     *
     * @param year year
     * @param month month
     * @param day day of the month (1-31, range depends on the month and year)
     * @param hour hour (0 - 23)
     * @param minute minute (0 - 59)
     * @param second second (0 - 59)
     * @return created time
     */
    public static Date createTime(int year, Month month, int day, int hour, int minute, int second) {
        return createTime(year, month, day, hour, minute, second, 0);
    }

    /**
     * Creates time according the Gregorian calendar input.
     *
     * @param year year
     * @param month month
     * @param day day of the month (1-31, range depends on the month and year)
     * @param hour hour (0 - 23)
     * @param minute minute (0 - 59)
     * @param second second (0 - 59)
     * @param millisecond millisecond (0-999)
     * @return created time
     */
    public static Date createTime(int year, Month month, int day, int hour, int minute, int second, int millisecond) {
        Calendar calendar = new GregorianCalendar();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        switch (month) {
        case JANUARY:
            calendar.set(Calendar.MONTH, 0);
            break;
        case FEBRUARY:
            calendar.set(Calendar.MONTH, 1);
            break;
        case MARCH:
            calendar.set(Calendar.MONTH, 2);
            break;
        case APRIL:
            calendar.set(Calendar.MONTH, 3);
            break;
        case MAY:
            calendar.set(Calendar.MONTH, 4);
            break;
        case JUNE:
            calendar.set(Calendar.MONTH, 5);
            break;
        case JULY:
            calendar.set(Calendar.MONTH, 6);
            break;
        case AUGUST:
            calendar.set(Calendar.MONTH, 7);
            break;
        case SEPTEMBER:
            calendar.set(Calendar.MONTH, 8);
            break;
        case OCTOBER:
            calendar.set(Calendar.MONTH, 9);
            break;
        case NOVEMBER:
            calendar.set(Calendar.MONTH, 10);
            break;
        case DECEMBER:
            calendar.set(Calendar.MONTH, 11);
            break;
        default:
            throw new IllegalArgumentException("unknown month: month = " + month);
        }
        calendar.set(Calendar.DATE, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);
        calendar.set(Calendar.MILLISECOND, millisecond);
        return calendar.getTime();
    }

    /**
     * Takes the time in a given date and returns how many minutes past from the
     * specified day from 0 : 0 : 0.
     * Examples:
     * <ul>
     * <li>1st January 2012 00 : 00 : 00 =&gt; 0</li>
     * <li>1st January 2012 00 : 00 : 14 =&gt; 0</li>
     * <li>1st January 2012 00 : 59 : 14 =&gt; 59</li>
     * <li>1st January 2012 01 : 00 : 35 =&gt; 60</li>
     * <li>1st January 2012 01 : 12 : 35 =&gt; 72</li>
     * <li>1st January 2012 23 : 59 : 59 =&gt; 1439 (23 * 60 + 59)</li>
     * </ul>
     *
     * @param date input date
     * @return how many minutes past from the noon of the day in the specified timestamp
     */
    public static int getMinuteOfDay(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar.get(Calendar.HOUR_OF_DAY) * 60 + calendar.get(Calendar.MINUTE);
    }

    /**
     * Takes the time in a given date and returns how many hours past from the
     * specified day from 0 : 0 : 0.
     * Examples:
     * <ul>
     * <li>1st January 2012 00 : 00 : 00 =&gt; 0</li>
     * <li>1st January 2012 00 : 00 : 14 =&gt; 0</li>
     * <li>1st January 2012 00 : 59 : 14 =&gt; 0</li>
     * <li>1st January 2012 01 : 00 : 35 =&gt; 1</li>
     * <li>1st January 2012 01 : 12 : 35 =&gt; 1</li>
     * <li>1st January 2012 23 : 59 : 59 =&gt; 23</li>
     * </ul>
     *
     * @param date input date
     * @return how many hours past from the noon of the day in the specified timestamp
     */
    public static int getHourOfDay(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    /**
     * Takes the time in a given date and returns how many days past from the beginning of the year.
     * This is value in range &lt;1, 365&gt; for the year which does not have 29 February and value
     * in rage &lt;1, 366&gt; for the year which has 29 February.
     * Examples:
     * <ul>
     * <li>1st January 2011 =&gt; 1</li>
     * <li>31th January 2011 =&gt; 31</li>
     * <li>1st August 2011 =&gt; 213</li>
     * <li>31st December 2011 =&gt; 365</li>
     * <li>1st January 2012 =&gt; 1</li>
     * <li>31th January 2012 =&gt; 31</li>
     * <li>1st August 2012 =&gt; 214</li>
     * <li>31st December 2012 =&gt; 366</li>
     * </ul>
     *
     * @param date input date
     * @return how many days past from the beginning of the year
     */
    public static int getDayOfYear(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_YEAR);
    }

    /**
     * Returns year.
     *
     * @param date input date
     * @return year
     */
    public static int getYear(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR);
    }

    /**
     * Returns month.
     *
     * @param date input date
     * @return month
     */
    public static Month getMonth(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH);
        switch (month) {
        case 0:
            return Month.JANUARY;
        case 1:
            return Month.FEBRUARY;
        case 2:
            return Month.MARCH;
        case 3:
            return Month.APRIL;
        case 4:
            return Month.MAY;
        case 5:
            return Month.JUNE;
        case 6:
            return Month.JULY;
        case 7:
            return Month.AUGUST;
        case 8:
            return Month.SEPTEMBER;
        case 9:
            return Month.OCTOBER;
        case 10:
            return Month.NOVEMBER;
        case 11:
            return Month.DECEMBER;
        default:
            throw new IllegalArgumentException("unknown month: month = " + month);
        }
    }

    /**
     * Returns day of month. Starting from 1.
     *
     * @param date input date
     * @return day of month, starting from 1
     */
    public static int getDayOfMonth(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Returns day of week.
     *
     * @param date input date
     * @return day of week
     */
    public static DayOfWeek getDayOfWeek(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int dow = calendar.get(Calendar.DAY_OF_WEEK);
        switch (dow) {
        case 1:
            return DayOfWeek.SUNDAY;
        case 2:
            return DayOfWeek.MONDAY;
        case 3:
            return DayOfWeek.TUESDAY;
        case 4:
            return DayOfWeek.WEDNESDAY;
        case 5:
            return DayOfWeek.THURSDAY;
        case 6:
            return DayOfWeek.FRIDAY;
        case 7:
            return DayOfWeek.SATURDAY;
        default:
            throw new IllegalArgumentException("unknown day of the week: day of the week = " + dow);
        }
    }

    /**
     * Truncates date to the second.
     *
     * @param date input date
     * @return truncated date
     */
    public static Date truncToSecond(Date date) {
        return DateUtils.truncate(date, Calendar.SECOND);
    }

    /**
     * Truncates date to the minute.
     *
     * @param date input date
     * @return truncated date
     */
    public static Date truncToMinute(Date date) {
        return DateUtils.truncate(date, Calendar.MINUTE);
    }

    /**
     * Truncates date to the hour.
     *
     * @param date input date
     * @return truncated date
     */
    public static Date truncToHour(Date date) {
        return DateUtils.truncate(date, Calendar.HOUR);
    }

    /**
     * Truncates date to the day.
     *
     * @param date input date
     * @return truncated date
     */
    public static Date truncToDay(Date date) {
        return DateUtils.truncate(date, Calendar.DATE);
    }

    /**
     * Truncates date to the month.
     *
     * @param date input date
     * @return truncated date
     */
    public static Date truncToMonth(Date date) {
        return DateUtils.truncate(date, Calendar.MONTH);
    }

    /**
     * Truncates date to the year.
     *
     * @param date input date
     * @return truncated date
     */
    public static Date truncToYear(Date date) {
        return DateUtils.truncate(date, Calendar.YEAR);
    }

    /**
     * Returns how many days the input dates are apart. Only the date and more
     * significant fields are included in the calculation. The formula is:
     * numberOfDays(truncToDays(second) - truncToDays(first)).
     * Examples:
     * <ul>
     * <li>(1st January 2012 00 : 00 : 00, 1st January 2012 00 : 00 : 00) =&gt; 0</li>
     * <li>(1st January 2012 00 : 00 : 00, 1st January 2012 23 : 59 : 59) =&gt; 0</li>
     * <li>(1st January 2012 23 : 59 : 59, 1st January 2012 00 : 00 : 00) =&gt; 0</li>
     * <li>(1st January 2012 00 : 00 : 00, 2nd January 2012 00 : 00 : 00) =&gt; 1</li>
     * <li>(1st January 2012 23 : 59 : 59, 2nd January 2012 00 : 00 : 00) =&gt; 1</li>
     * <li>(1st January 2012 23 : 59 : 59, 2nd January 2012 23 : 59 : 59) =&gt; 1</li>
     * <li>(2nd January 2012 00 : 00 : 00, 1st January 2012 23 : 59 : 59) =&gt; -1</li>
     * <li>(1st January 2012 00 : 00 : 00, 1st February 2012 00 : 00 : 00) =&gt; 31</li>
     * </ul>
     *
     * @param first first date
     * @param second second date
     * @return the days difference
     */
    public static int getDayDiff(Date first, Date second) {
        Date firstDay = DateUtils.truncate(first, Calendar.DATE);
        Date secondDay = DateUtils.truncate(second, Calendar.DATE);
        return Long.valueOf((secondDay.getTime() - firstDay.getTime()) / 1000 / 60 / 60 / 24).intValue();
    }

    /**
     * Formats date into string.
     *
     * @param date date which will be formatted
     * @param pattern pattern which will be used
     * @return string with formatted date
     */
    public static String format(Date date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);
    }

    /**
     * Parses date.
     *
     * @param date date string which will be parsed
     * @param patterns patterns which will be used, result is returned from the first pattern which can parse the string
     * @return created date
     */
    public static Date parse(String date, String... patterns) {
        for (String pattern : patterns) {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            try {
                return sdf.parse(date);
            } catch (ParseException e) {
                // ignore this as another pattern is in use
                // if no pattern matches, then exception is thrown t the end of the loop
            }
        }
        throw new IllegalArgumentException("no pattern matches the date format: " +
                "date = " + date + ", patterns = " + Arrays.asList(patterns));
    }

}
