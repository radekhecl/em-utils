package com.enterprisemath.utils.http;

/**
 * Available request methods.
 *
 * @author radek.hecl
 */
public enum HttpReqMethod {

    /**
     * GET method.
     */
    GET,
    /**
     * POST method.
     */
    POST,
    /**
     * PUT method.
     */
    PUT,
    /**
     * DELETE method.
     */
    DELETE

}
