package com.enterprisemath.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * Id generator which produces UUIDs in in the sequence.
 * The first UUID is 00000000-0000-0000-0000-000000000001.
 *
 * @author radek.hecl
 */
public class SequenceUuidGenerator implements IdGenerator {

    /**
     * Common prefix.
     */
    private static final String PREFIX = "00000000-0000-0000-0000-";
    
    /**
     * Next number.
     */
    private int next = 1;

    /**
     * Object for synchronization.
     */
    private final Object lock = new Object();

    /**
     * Creates new instance.
     *
     * @param builder builder object
     */
    private SequenceUuidGenerator() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
    }

    @Override
    public String generateId() {
        synchronized (lock) {
            String res = PREFIX + StringUtils.leftPad(String.valueOf(next), 12, "0");
            next = next + 1;
            return res;
        }
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @return created instance
     */
    public static SequenceUuidGenerator create() {
        SequenceUuidGenerator res = new SequenceUuidGenerator();
        res.guardInvariants();
        return res;
    }
}
