package com.enterprisemath.utils;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Utility class which allows to parse strings.
 * Instance is created on the fly and methods for parsing are exposed.
 *
 * @author radek.hecl
 */
public class StringParser {

    /**
     * Input string.
     */
    private String input;

    /**
     * Cursor position.
     */
    private int cursor = 0;

    /**
     * Creates new instance.
     *
     * @param input input string which will be parsed
     */
    public StringParser(String input) {
        this.input = input;
        guardInvariants();
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        Guard.notNull(input, "input cannot be null");
    }

    /**
     * Returns true if next character is available to read.
     * False if end of input string has been reached.
     *
     * @return true if there is next character, false otherwise
     */
    public boolean hasNext() {
        return cursor < input.length();
    }

    /**
     * Reads one character. Cursor is moved by one character.
     *
     * @return character
     * @throws IndexOutOfBoundsException if there is no next character
     */
    public String readCharacter() {
        String res = input.substring(cursor, cursor + 1);
        ++cursor;
        return res;
    }

    /**
     * Reads one character. Cursor is moved by one character.
     *
     * @param def default value if parser is at the end
     * @return character or default end string
     */
    public String readCharacter(String def) {
        if (cursor >= input.length()) {
            return def;
        }
        String res = input.substring(cursor, cursor + 1);
        ++cursor;
        return res;
    }

    /**
     * Looks up character without moving the cursor.
     *
     * @return character
     * @throws IndexOutOfBoundsException if there is no next character
     */
    public String lookupCharacter() {
        return input.substring(cursor, cursor + 1);
    }

    /**
     * Looks up character without moving the cursor.
     *
     * @param def default value if parser is at the end
     * @return character or default end string
     */
    public String lookupCharacter(String def) {
        if (cursor >= input.length()) {
            return def;
        }
        return input.substring(cursor, cursor + 1);
    }

    /**
     * Reads until one of the stop character or end of the string is reached.
     * Cursor is moved by the length of the result string. Next character will be the one which caused stop.
     *
     * @param stopCharacters set of stop characters
     * @return sequence before the stop character
     */
    public String readTillStop(String stopCharacters) {
        StringBuilder res = new StringBuilder();
        while (cursor < input.length()) {
            String c = input.substring(cursor, cursor + 1);
            if (stopCharacters.contains(c)) {
                break;
            }
            ++cursor;
            res.append(c);
        }
        return res.toString();
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
