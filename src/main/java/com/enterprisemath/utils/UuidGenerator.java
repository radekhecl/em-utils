package com.enterprisemath.utils;

import java.util.UUID;

/**
 * Id generator which produces UUID strings.
 *
 * @author radek.hecl
 */
public class UuidGenerator implements IdGenerator {

    /**
     * Object for synchronization.
     */
    private final Object lock = new Object();

    /**
     * Creates new instance.
     *
     * @param builder builder object
     */
    private UuidGenerator() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
    }

    @Override
    public String generateId() {
        synchronized (lock) {
            return UUID.randomUUID().toString();
        }
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @return created instance
     */
    public static UuidGenerator create() {
        UuidGenerator res = new UuidGenerator();
        res.guardInvariants();
        return res;
    }
}
