package com.enterprisemath.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 * Class which contains utility methods to work with CSV files.
 *
 * @author radek.hecl
 */
public final class Csv {

    /**
     * Prevents construction.
     */
    private Csv() {
    }

    /**
     * Parses data. Encoding is UFT-8.
     *
     * @param data data
     * @return csv records
     */
    public static List<List<String>> parse(byte[] data) {
        List<String> lines = null;
        try (ByteArrayInputStream bis = new ByteArrayInputStream(data)) {
            lines = readLines(bis);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        List<List<String>> res = new ArrayList<>(lines.size() - 1);
        List<String> csvLine = new ArrayList<>();
        boolean escapedCell = false;
        StringBuilder cellBuilder = null;
        for (int l = 0; l < lines.size(); ++l) {
            StringParser parser = new StringParser(lines.get(l));
            while (parser.hasNext()) {
                if (cellBuilder == null) {
                    String chr = parser.readCharacter();
                    if (chr.equals(",")) {
                        csvLine.add("");
                    }
                    else if (chr.equals("\"")) {
                        escapedCell = true;
                        cellBuilder = new StringBuilder();
                    }
                    else {
                        escapedCell = false;
                        cellBuilder = new StringBuilder();
                        cellBuilder.append(chr);
                    }
                }
                else {
                    if (escapedCell) {
                        String str = parser.readTillStop("\"");
                        cellBuilder.append(str);
                        if (parser.hasNext()) {
                            parser.readCharacter();
                            if (parser.hasNext()) {
                                String next = parser.readCharacter();
                                if (next.equals("\"")) {
                                    cellBuilder.append("\"");
                                }
                                else if (next.equals(",")) {
                                    csvLine.add(cellBuilder.toString());
                                    cellBuilder = null;
                                }
                            }
                            else {
                                csvLine.add(cellBuilder.toString());
                                cellBuilder = null;
                            }
                        }
                    }
                    else {
                        String str = parser.readTillStop(",");
                        cellBuilder.append(str);
                        csvLine.add(cellBuilder.toString());
                        cellBuilder = null;
                        if (parser.hasNext()) {
                            parser.readCharacter();
                        }
                    }
                }
            }
            if (cellBuilder == null) {
                res.add(csvLine);
                csvLine = new ArrayList<>();
            }
            else {
                cellBuilder.append("\n");
            }
        }
        if (cellBuilder != null) {
            throw new IllegalArgumentException("last cell in file is not closed: line = " + lines.size());
        }
        return res;
    }

    /**
     * Formats data. Encoding is UFT-8.
     *
     * @param data data
     * @return csv file
     */
    public static byte[] format(List<List<String>> data) {
        StringBuilder res = new StringBuilder();
        for (List<String> line : data) {
            for (int i = 0; i < line.size(); ++i) {
                String cell = StringUtils.defaultString(line.get(i));
                cell = cell.replaceAll("\"", "\"\"");
                res.append("\"").append(cell).append("\"");
                if (i == line.size() - 1) {
                    res.append("\n");
                }
                else {
                    res.append(",");
                }
            }
        }
        return res.toString().getBytes(Charset.forName("utf-8"));
    }

    /**
     * Reads line of the input stream. Expected is UTF-8 encoding.
     *
     * @param input input steam
     * @return string lines
     * @throws IOException in case some error happens
     */
    private static List<String> readLines(InputStream input) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(input, Charset.forName("utf-8")));
        final List<String> list = new ArrayList<>();
        String line = reader.readLine();
        while (line != null) {
            list.add(line);
            line = reader.readLine();
        }
        return list;
    }

}
