package com.enterprisemath.utils.http;

import com.enterprisemath.utils.Guard;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.json.JSONObject;
import org.junit.After;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

/**
 * Test case for default HTTP connector.
 *
 * @author radek.hecl
 */
public class DefaultHttpConnectorTest {

    /**
     * Port for the embedded test server.
     */
    private static final int SERVER_PORT = 43239;

    /**
     * Base URL for all tests.
     */
    private static final String URL = "http://localhost:" + SERVER_PORT;

    /**
     * Test server.
     */
    private Server server;

    /**
     * Creates new instance.
     */
    public DefaultHttpConnectorTest() {
    }

    /**
     * Sets up the test environment.
     */
    @Before
    public void setUp() {
        server = new Server(SERVER_PORT);
        server.setHandler(new TestHandler());
        try {
            server.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Tears down the test environment.
     */
    @After
    public void tearDown() {
        try {
            server.stop();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Tests request which returns string.
     */
    @Test
    public void testExecuteString() {
        DefaultHttpConnector connector = DefaultHttpConnector.create();

        String res = null;

        res = connector.executeString(GetReq.create(URL + "/hello"));
        assertEquals("hello baby", res);

        // url parameters
        res = connector.executeString(GetReq.create(URL + "/hello-n?number=3&lastEol=false"));
        assertEquals("hello baby\nhello baby\nhello baby", res);

        res = connector.executeString(GetReq.create(URL + "/hello-n?number=3&lastEol=true"));
        assertEquals("hello baby\nhello baby\nhello baby\n", res);

        // headers
        res = connector.executeString(GetReq.create(URL + "/hello-n-header",
                Arrays.asList(HttpHeader.create("number", "3"), HttpHeader.create("lastEol", "false"))));
        assertEquals("hello baby\nhello baby\nhello baby", res);

        res = connector.executeString(GetReq.create(URL + "/hello-n-header",
                Arrays.asList(HttpHeader.create("number", "3"), HttpHeader.create("lastEol", "true"))));
        assertEquals("hello baby\nhello baby\nhello baby\n", res);

        // in the body
        res = connector.executeString(PostReq.createUrlBody(URL + "/sum-url-encoded", "number1", "5", "number2", "7"));
        assertEquals("12", res);

        res = connector.executeString(PostReq.createJsonBody(URL + "/sum-json", "{\"number1\" : 5, \"number2\" : 9}"));
        assertEquals("14", res);

        // gzip
        res = connector.executeString(GetReq.create(URL + "/hello-gzip"));
        assertEquals("hello gzip", res);

        // error
        try {
            connector.executeString(GetReq.create(URL + "/hello-error"));
            fail("exception expected");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage(), e.getMessage().contains("Response code is not success (2XX): 500"));
        }
    }

    /**
     * Tests request which returns bytes.
     *
     * @throws java.io.UnsupportedEncodingException if UTF-8 character set is not available
     */
    @Test
    public void testExecuteBytes() throws UnsupportedEncodingException {
        DefaultHttpConnector connector = DefaultHttpConnector.create();

        byte[] res = null;

        res = connector.executeBytes(GetReq.create(URL + "/hello"));
        assertArrayEquals("hello baby".getBytes("utf-8"), res);

        // url parameters
        res = connector.executeBytes(GetReq.create(URL + "/hello-n?number=3&lastEol=false"));
        assertArrayEquals("hello baby\nhello baby\nhello baby".getBytes("utf-8"), res);

        res = connector.executeBytes(GetReq.create(URL + "/hello-n?number=3&lastEol=true"));
        assertArrayEquals("hello baby\nhello baby\nhello baby\n".getBytes("utf-8"), res);

        // headers
        res = connector.executeBytes(GetReq.create(URL + "/hello-n-header",
                Arrays.asList(HttpHeader.create("number", "3"), HttpHeader.create("lastEol", "false"))));
        assertArrayEquals("hello baby\nhello baby\nhello baby".getBytes("utf-8"), res);

        res = connector.executeBytes(GetReq.create(URL + "/hello-n-header",
                Arrays.asList(HttpHeader.create("number", "3"), HttpHeader.create("lastEol", "true"))));
        assertArrayEquals("hello baby\nhello baby\nhello baby\n".getBytes("utf-8"), res);

        // in the body
        res = connector.executeBytes(PostReq.createUrlBody(URL + "/sum-url-encoded", "number1", "5", "number2", "7"));
        assertArrayEquals("12".getBytes("utf-8"), res);

        res = connector.executeBytes(PostReq.createJsonBody(URL + "/sum-json", "{\"number1\" : 5, \"number2\" : 9}"));
        assertArrayEquals("14".getBytes("utf-8"), res);

        // gzip
        res = connector.executeBytes(GetReq.create(URL + "/hello-gzip"));
        assertArrayEquals("hello gzip".getBytes("utf-8"), res);

        // error
        try {
            connector.executeBytes(GetReq.create(URL + "/hello-error"));
            fail("exception expected");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage(), e.getMessage().contains("Response code is not success (2XX): 500"));
        }
    }

    /**
     * Tests request which returns full response.
     *
     * @throws java.io.UnsupportedEncodingException if UTF-8 character set is not available
     */
    @Test
    public void testExecute() throws UnsupportedEncodingException {
        DefaultHttpConnector connector = DefaultHttpConnector.create();

        List<HttpHeader> headers = null;
        HttpResp expected = null;
        HttpResp res = null;

        // url parameters
        res = connector.execute(GetReq.create(URL + "/hello-n?number=3&lastEol=false"));
        headers = Arrays.asList(
                HttpHeader.create("Content-Length", String.valueOf("hello baby\nhello baby\nhello baby".getBytes("utf-8").length)),
                HttpHeader.create("Content-Type", "text/text; charset=utf-8"),
                HttpHeader.create("Date", res.getHeader("Date")),
                HttpHeader.create("Server", "Jetty(9.4.45.v20220203)"));
        expected = HttpResp.create(200, headers, "hello baby\nhello baby\nhello baby".getBytes("utf-8"));
        assertEquals(expected, res);

        // in the body
        res = connector.execute(PostReq.createUrlBody(URL + "/sum-url-encoded", "number1", "5", "number2", "7"));
        headers = Arrays.asList(
                HttpHeader.create("Content-Length", String.valueOf("12".getBytes("utf-8").length)),
                HttpHeader.create("Content-Type", "text/text; charset=utf-8"),
                HttpHeader.create("Date", res.getHeader("Date")),
                HttpHeader.create("Server", "Jetty(9.4.45.v20220203)"));
        expected = HttpResp.create(200, headers, "12".getBytes("utf-8"));
        assertEquals(expected, res);

        res = connector.execute(PostReq.createJsonBody(URL + "/sum-json", "{\"number1\" : 5, \"number2\" : 9}"));
        headers = Arrays.asList(
                HttpHeader.create("Content-Length", String.valueOf("14".getBytes("utf-8").length)),
                HttpHeader.create("Content-Type", "text/text; charset=utf-8"),
                HttpHeader.create("Date", res.getHeader("Date")),
                HttpHeader.create("Server", "Jetty(9.4.45.v20220203)"));
        expected = HttpResp.create(200, headers, "14".getBytes("utf-8"));
        assertEquals(expected, res);

        // gzip
        res = connector.execute(GetReq.create(URL + "/hello-gzip"));
        headers = Arrays.asList(
                HttpHeader.create("Content-Encoding", "gzip"),
                HttpHeader.create("Content-Length", "30"),
                HttpHeader.create("Content-Type", "text/text; charset=utf-8"),
                HttpHeader.create("Date", res.getHeader("Date")),
                HttpHeader.create("Server", "Jetty(9.4.45.v20220203)"));
        expected = HttpResp.create(200, headers, res.getBody());
        assertEquals(expected, res);
        assertEquals("hello gzip", res.getBodyString());

        // error
        res = connector.execute(GetReq.create(URL + "/hello-error"));
        headers = Arrays.asList(
                HttpHeader.create("Content-Length", String.valueOf("hello error".getBytes("utf-8").length)),
                HttpHeader.create("Content-Type", "text/text; charset=utf-8"),
                HttpHeader.create("Date", res.getHeader("Date")),
                HttpHeader.create("Server", "Jetty(9.4.45.v20220203)"));
        expected = HttpResp.create(500, headers, "hello error".getBytes("utf-8"));
        assertEquals(expected, res);

        res = connector.execute(GetReq.create(URL + "/denied"));
        headers = Arrays.asList(
                HttpHeader.create("Content-Length", String.valueOf("denied".getBytes("utf-8").length)),
                HttpHeader.create("Content-Type", "text/text; charset=utf-8"),
                HttpHeader.create("Date", res.getHeader("Date")),
                HttpHeader.create("Server", "Jetty(9.4.45.v20220203)"));
        expected = HttpResp.create(403, headers, "denied".getBytes("utf-8"));
        assertEquals(expected, res);
    }

    /**
     * Tests read timeout in the request.
     */
    @Test
    public void testReadTimeout() {
        DefaultHttpConnector connector = DefaultHttpConnector.create();

        String res = null;

        // no read timeout set
        res = connector.executeString(GetReq.create(URL + "/hello-timeout", "timeout", "2000"));
        assertEquals("hello baby", res);

        // timeout limited
        HttpReq req = new HttpReq.Builder().
                setUrl(URL + "/hello-timeout?timeout=2000").
                setMethod(HttpReqMethod.GET).
                putInstruction(HttpReq.READ_TIMEOUT, "1000").
                build();
        try {
            connector.executeString(req);
            fail("exception expected");
        } catch (RuntimeException e) {
            assertNotNull(e.getMessage() + " is not the expected", e.getCause());
            assertTrue(e.getCause().toString(), e.getCause() instanceof SocketTimeoutException);
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Dedicated test handler.
     */
    private static class TestHandler extends AbstractHandler {

        /**
         * Creates new instance.
         */
        public TestHandler() {
        }

        @Override
        public void handle(String string, Request rqst, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
            if (request.getRequestURI().equals("/hello")) {
                response.setCharacterEncoding("utf-8");
                byte[] res = "hello baby".getBytes("utf-8");
                response.setContentLength(res.length);
                response.getOutputStream().write(res);
            }
            else if (request.getRequestURI().equals("/hello-timeout")) {
                int timeout = Integer.valueOf(request.getParameter("timeout"));
                try {
                    Thread.sleep(timeout);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    throw new RuntimeException(ex);
                }
                response.setCharacterEncoding("utf-8");
                byte[] res = "hello baby".getBytes("utf-8");
                response.setContentLength(res.length);
                response.getOutputStream().write(res);
            }
            else if (request.getRequestURI().equals("/hello-n")) {
                int num = Integer.valueOf(request.getParameter("number"));
                boolean lastEol = Boolean.valueOf(request.getParameter("lastEol"));
                StringBuilder str = new StringBuilder();
                for (int i = 0; i < num; ++i) {
                    str.append("hello baby");
                    if (i < num - 1 || lastEol) {
                        str.append("\n");
                    }
                }
                response.setCharacterEncoding("utf-8");
                byte[] res = str.toString().getBytes("utf-8");
                response.setHeader("Content-Type", "text/text; charset=utf-8");
                response.setContentLength(res.length);
                response.getOutputStream().write(res);
            }
            else if (request.getRequestURI().equals("/hello-n-header")) {
                int num = Integer.valueOf(request.getHeader("number"));
                boolean lastEol = Boolean.valueOf(request.getHeader("lastEol"));
                StringBuilder str = new StringBuilder();
                for (int i = 0; i < num; ++i) {
                    str.append("hello baby");
                    if (i < num - 1 || lastEol) {
                        str.append("\n");
                    }
                }
                response.setCharacterEncoding("utf-8");
                byte[] res = str.toString().getBytes("utf-8");
                response.setHeader("Content-Type", "text/text; charset=utf-8");
                response.setContentLength(res.length);
                response.getOutputStream().write(res);
            }
            else if (request.getRequestURI().equals("/sum-url-encoded")) {
                Guard.equals("POST", request.getMethod(), "invalid method");
                int num1 = Integer.valueOf(request.getParameter("number1"));
                int num2 = Integer.valueOf(request.getParameter("number2"));
                int sum = num1 + num2;
                response.setCharacterEncoding("utf-8");
                byte[] res = String.valueOf(sum).getBytes("utf-8");
                response.setHeader("Content-Type", "text/text; charset=utf-8");
                response.setContentLength(res.length);
                response.getOutputStream().write(res);
            }
            else if (request.getRequestURI().equals("/sum-json")) {
                Guard.equals("POST", request.getMethod(), "invalid method");
                List<String> lines = IOUtils.readLines(request.getReader());
                String str = StringUtils.join(lines, "");
                JSONObject json = new JSONObject(str);
                int num1 = json.getInt("number1");
                int num2 = json.getInt("number2");
                int sum = num1 + num2;
                response.setCharacterEncoding("utf-8");
                byte[] res = String.valueOf(sum).getBytes("utf-8");
                response.setHeader("Content-Type", "text/text; charset=utf-8");
                response.setContentLength(res.length);
                response.getOutputStream().write(res);
            }
            else if (request.getRequestURI().equals("/hello-error")) {
                response.setStatus(500);
                byte[] res = "hello error".getBytes("utf-8");
                response.setHeader("Content-Type", "text/text; charset=utf-8");
                response.setContentLength(res.length);
                response.getOutputStream().write(res);
            }
            else if (request.getRequestURI().equals("/hello-gzip")) {
                byte[] res = gzip("hello gzip".getBytes("utf-8"));
                response.setCharacterEncoding("utf-8");
                response.setHeader("Content-Type", "text/text; charset=utf-8");
                response.setHeader("Content-Encoding", "gzip");
                response.setContentLength(res.length);
                response.getOutputStream().write(res);
            }
            else if (request.getRequestURI().equals("/denied")) {
                response.setStatus(403);
                byte[] res = "denied".getBytes("utf-8");
                response.setHeader("Content-Type", "text/text; charset=utf-8");
                response.setContentLength(res.length);
                response.getOutputStream().write(res);
            }
            else {
                throw new IllegalArgumentException("unknown URI: " + request.getRequestURI());
            }
        }

        /**
         * Preforms gzip operation on the specified data field.
         *
         * @param buf buffer
         * @return gzip
         */
        private byte[] gzip(byte[] buf) {
            try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    GZIPOutputStream gos = new GZIPOutputStream(bos)) {
                gos.write(buf);
                gos.close();
                bos.close();
                return bos.toByteArray();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }

    }

}
