package com.enterprisemath.utils.http;

import com.enterprisemath.utils.Dut;
import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the HTTP connector which is using standard java URL object for connectivity.
 *
 * @author radek.hecl
 */
public class DefaultHttpConnector implements HttpConnector {

    /**
     * Whether to ignore validity of security certificates or not.
     */
    private boolean ignoreCerts = false;

    /**
     * Creates new instance.
     */
    private DefaultHttpConnector() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
    }

    @Override
    public String executeString(HttpReq request) {
        HttpResp resp = execute(request);
        if (!resp.is2XX()) {
            throw new RuntimeException("Response code is not success (2XX): " + resp.getCode());
        }
        return resp.getBodyString();
    }

    @Override
    public byte[] executeBytes(HttpReq request) {
        HttpResp resp = execute(request);
        if (!resp.is2XX()) {
            throw new RuntimeException("Response code is not success (2XX): " + resp.getCode());
        }
        return resp.getDecompressedBody();
    }

    @Override
    public HttpResp execute(HttpReq request) {
        DataOutputStream wr = null;
        InputStream in = null;
        ByteArrayOutputStream bos = null;
        int responseCode = -1;
        List<HttpHeader> headers = new ArrayList<>();
        try {
            URL obj = new URL(request.getUrl());
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            if (con instanceof HttpsURLConnection && isIgnoreCerts(request.getInstructions())) {
                setUpTrustAllCerts((HttpsURLConnection) con);
            }
            con.setRequestMethod(request.getMethod().name());
            if (request.getInstructions().containsKey(HttpReq.READ_TIMEOUT)) {
                con.setReadTimeout(Integer.valueOf(request.getInstructions().get(HttpReq.READ_TIMEOUT)));
            }
            for (HttpHeader header : request.getHeaders()) {
                con.setRequestProperty(header.getName(), header.getValue());
            }

            byte[] body = request.getBody();
            if (body != null && body.length > 0) {
                con.setDoOutput(true);
                wr = new DataOutputStream(con.getOutputStream());
                wr.write(body);
                wr.flush();
                wr.close();
            }

            responseCode = con.getResponseCode();
            Map<String, List<String>> hhh = Dut.copyMap(con.getHeaderFields());
            hhh.remove(null);
            hhh = Dut.copySortedMap(hhh);
            for (String key : hhh.keySet()) {
                for (String value : hhh.get(key)) {
                    headers.add(HttpHeader.create(key, value));
                }
            }

            bos = new ByteArrayOutputStream();
            if (responseCode >= 200 && responseCode < 300) {
                in = con.getInputStream();
            }
            else {
                in = con.getErrorStream();
            }
            if (in != null) {
                byte[] buf = new byte[1024];
                int r = 1;
                while (r != -1) {
                    r = in.read(buf);
                    if (r != -1) {
                        bos.write(buf, 0, r);
                    }
                }
                in.close();
            }
            bos.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            closeQuietly(in);
            closeQuietly(bos);
        }
        return HttpResp.create(responseCode, headers, bos.toByteArray());
    }

    /**
     * Returns whether certificates should be ignored or not.
     *
     * @param reqInstructions request instructions
     * @return true if certificates should be ignored
     */
    private boolean isIgnoreCerts(Map<String, String> reqInstructions) {
        if (reqInstructions.containsKey(HttpReq.IGNORE_CERTS)) {
            return Boolean.valueOf(reqInstructions.get(HttpReq.IGNORE_CERTS));
        }
        return ignoreCerts;
    }

    /**
     * Sets up connections to trust all certificates.
     *
     * @param httpsCon connection
     */
    private void setUpTrustAllCerts(HttpsURLConnection httpsCon) {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(
                    java.security.cert.X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(
                    java.security.cert.X509Certificate[] certs, String authType) {
            }
        }};

        SSLContext sc;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (KeyManagementException e) {
            throw new RuntimeException(e);
        }
        httpsCon.setSSLSocketFactory(sc.getSocketFactory());
        HostnameVerifier allHostsValid = (String hostname, SSLSession session) -> true;
        httpsCon.setHostnameVerifier(allHostsValid);
    }

    /**
     * Closes quietly the specified object.
     * Possible IOException is ignored.
     *
     * @param closeable closable object
     */
    private void closeQuietly(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (final IOException ioe) {
            // ignore
        }
    }

    /**
     * Creates new instance.
     *
     * @return created object
     */
    public static DefaultHttpConnector create() {
        DefaultHttpConnector res = new DefaultHttpConnector();
        res.guardInvariants();
        return res;
    }

    /**
     * Creates new instance.
     *
     * @param ignoreCerts whether to ignore validity of security certificates or not
     * @return created object
     */
    public static DefaultHttpConnector create(boolean ignoreCerts) {
        DefaultHttpConnector res = new DefaultHttpConnector();
        res.ignoreCerts = ignoreCerts;
        res.guardInvariants();
        return res;
    }

}
