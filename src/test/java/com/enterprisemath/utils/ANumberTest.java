package com.enterprisemath.utils;

import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * Test case which proves the arbitrary number.
 *
 * @author radek.hecl
 */
public class ANumberTest {

    /**
     * Creates new instance.
     */
    public ANumberTest() {
    }

    /**
     * Tests creation.
     */
    @Test
    public void testCreate() {
        assertEquals(ANumber.create(1), ANumber.create("1"));
        assertEquals(ANumber.create(-1), ANumber.create("-1"));
        assertEquals(ANumber.create(1), ANumber.create("1.00"));
        assertEquals(ANumber.create(1.5), ANumber.create("1.5"));
        assertEquals(ANumber.create(4.95), ANumber.create("4.95"));
        assertEquals(ANumber.create(new BigDecimal("1e-25")), ANumber.create("0.0000000000000000000000001"));
        assertEquals(ANumber.create(new BigDecimal("1e-25")), ANumber.create("0.000000000000000000000000100000"));

        //
        assertEquals(ANumber.create(1).hashCode(), ANumber.create("1").hashCode());
        assertEquals(ANumber.create(1).hashCode(), ANumber.create("1.00").hashCode());
        assertEquals(ANumber.create(1.5).hashCode(), ANumber.create("1.5").hashCode());
        assertEquals(ANumber.create(1000).hashCode(), ANumber.create("1000").hashCode());
        assertEquals(ANumber.create(1000).hashCode(), ANumber.create("1.000E3").hashCode());
        assertEquals(ANumber.create(new BigDecimal("1e-25")).hashCode(), ANumber.create("0.0000000000000000000000001").hashCode());
        assertEquals(ANumber.create(new BigDecimal("1e-25")).hashCode(), ANumber.create("0.000000000000000000000000100000").hashCode());
        assertEquals(ANumber.create(1000), ANumber.create("1000"));
        assertEquals(ANumber.create(1000), ANumber.create("1.000E3"));

        assertFalse(ANumber.create(1).hashCode() == ANumber.create("1.5").hashCode());
    }

    /**
     * Tests addition.
     */
    @Test
    public void testAdd() {
        assertEquals(ANumber.create(2), ANumber.create(1).add(ANumber.create("1")));

        // double version
        assertEquals(ANumber.create(2), ANumber.create(1).add(1));
    }

    /**
     * Tests subtraction.
     */
    @Test
    public void testSub() {
        assertEquals(ANumber.create(2), ANumber.create(3).sub(ANumber.create(1)));

        // double version
        assertEquals(ANumber.create(2), ANumber.create(3).sub(1));
    }

    /**
     * Tests multiplication.
     */
    @Test
    public void testMul() {
        assertEquals(ANumber.create(6), ANumber.create(3).mul(ANumber.create(2)));

        // double version
        assertEquals(ANumber.create(6), ANumber.create(3).mul(2));
    }

    @Test
    public void testMul10Pow() {
        assertEquals(ANumber.create(6), ANumber.create(6).mul10Pow(0));
        assertEquals(ANumber.create(60), ANumber.create(6).mul10Pow(1));
        assertEquals(ANumber.create(600), ANumber.create(6).mul10Pow(2));
        assertEquals(ANumber.create(0.6), ANumber.create(6).mul10Pow(-1));
        assertEquals(ANumber.create(0.06), ANumber.create(6).mul10Pow(-2));
    }

    /**
     * Tests division.
     */
    @Test
    public void testDiv() {
        assertEquals(ANumber.create("1.5"), ANumber.create(3).div(ANumber.create(2), 2));
        assertEquals(ANumber.create("0.66"), ANumber.create(2).div(ANumber.create(3), 2));
        assertEquals(ANumber.create("6.6"), ANumber.create(20).div(ANumber.create(3), 2));
        assertEquals(ANumber.create("0.6666666666"), ANumber.create(2).div(ANumber.create("3"), 10));

        // double version
        assertEquals(ANumber.create("1.5"), ANumber.create(3).div(2, 2));
        assertEquals(ANumber.create("0.66"), ANumber.create(2).div(3, 2));
        assertEquals(ANumber.create("6.6"), ANumber.create(20).div(3, 2));
        assertEquals(ANumber.create("0.6666666666"), ANumber.create(2).div(3, 10));
    }

    /**
     * Tests division to integral value.
     */
    @Test
    public void testDivToIntegralValue() {
        assertEquals(ANumber.create("1"), ANumber.create(3).divToIntegralValue(ANumber.create(2)));
        assertEquals(ANumber.create("0"), ANumber.create(2).divToIntegralValue(ANumber.create(3)));
        assertEquals(ANumber.create("6"), ANumber.create(20).divToIntegralValue(ANumber.create(3)));
        assertEquals(ANumber.create("16"), ANumber.create(50).divToIntegralValue(ANumber.create(3)));
        assertEquals(ANumber.create("-1"), ANumber.create(3).divToIntegralValue(ANumber.create(-2)));
        assertEquals(ANumber.create("-0"), ANumber.create(2).divToIntegralValue(ANumber.create(-3)));
        assertEquals(ANumber.create("-6"), ANumber.create(20).divToIntegralValue(ANumber.create(-3)));
        assertEquals(ANumber.create("-16"), ANumber.create(50).divToIntegralValue(ANumber.create(-3)));

        // double version
        assertEquals(ANumber.create("1"), ANumber.create(3).divToIntegralValue(2));
        assertEquals(ANumber.create("0"), ANumber.create(2).divToIntegralValue(3));
        assertEquals(ANumber.create("6"), ANumber.create(20).divToIntegralValue(3));
        assertEquals(ANumber.create("16"), ANumber.create(50).divToIntegralValue(3));
        assertEquals(ANumber.create("-1"), ANumber.create(3).divToIntegralValue(-2));
        assertEquals(ANumber.create("-0"), ANumber.create(2).divToIntegralValue(-3));
        assertEquals(ANumber.create("-6"), ANumber.create(20).divToIntegralValue(-3));
        assertEquals(ANumber.create("-16"), ANumber.create(50).divToIntegralValue(-3));
    }

    /**
     * Tests division with remainder.
     */
    @Test
    public void testDivWithRemainder() {
        assertEquals(ANumberRemainderPair.create(ANumber.create("1.5"), ANumber.create("0")),
                ANumber.create(3).divWithRemainder(ANumber.create(2), 2));
        assertEquals(ANumberRemainderPair.create(ANumber.create("0.66"), ANumber.create("0.02")),
                ANumber.create(2).divWithRemainder(ANumber.create(3), 2));
        assertEquals(ANumberRemainderPair.create(ANumber.create("6.6"), ANumber.create("0.2")),
                ANumber.create(20).divWithRemainder(ANumber.create(3), 2));
        assertEquals(ANumberRemainderPair.create(ANumber.create("0.6666666666"), ANumber.create("0.0000000002")),
                ANumber.create(2).divWithRemainder(ANumber.create(3), 10));

        // double version
        assertEquals(ANumberRemainderPair.create(ANumber.create("1.5"), ANumber.create("0")),
                ANumber.create(3).divWithRemainder(2, 2));
        assertEquals(ANumberRemainderPair.create(ANumber.create("0.66"), ANumber.create("0.02")),
                ANumber.create(2).divWithRemainder(3, 2));
        assertEquals(ANumberRemainderPair.create(ANumber.create("6.6"), ANumber.create("0.2")),
                ANumber.create(20).divWithRemainder(3, 2));
        assertEquals(ANumberRemainderPair.create(ANumber.create("0.6666666666"), ANumber.create("0.0000000002")),
                ANumber.create(2).divWithRemainder(3, 10));
    }

    /**
     * Tests negation.
     */
    @Test
    public void testNegate() {
        assertEquals(ANumber.create(0), ANumber.create(0).negate());
        assertEquals(ANumber.create(-1), ANumber.create(1).negate());
        assertEquals(ANumber.create(1), ANumber.create(-1).negate());
    }

    /**
     * Tests absolute value.
     */
    @Test
    public void testAbs() {
        assertEquals(ANumber.create(0), ANumber.create(0).abs());
        assertEquals(ANumber.create(1), ANumber.create(1).abs());
        assertEquals(ANumber.create(1), ANumber.create(-1).abs());
    }

    /**
     * Tests powering value.
     */
    @Test
    public void testPow() {
        assertEquals(ANumber.create(0), ANumber.create(0).pow(3, 2));
        assertEquals(ANumber.create(1), ANumber.create(1).pow(3, 2));
        assertEquals(ANumber.create(8), ANumber.create(2).pow(3, 2));
        assertEquals(ANumber.create(15.62), ANumber.create(2.50).pow(3, 4));
        assertEquals(ANumber.create(-15.625), ANumber.create(-2.50).pow(3, 10));
    }

    /**
     * Tests decimal truncation.
     */
    @Test
    public void testTruncDecimals() {
        assertEquals(ANumber.create("1"), ANumber.create("1").truncDecimals());
        assertEquals(ANumber.create("1"), ANumber.create("1.5").truncDecimals());
        assertEquals(ANumber.create("10"), ANumber.create("10").truncDecimals());
        assertEquals(ANumber.create("10"), ANumber.create("10.5").truncDecimals());
        assertEquals(ANumber.create("-1"), ANumber.create("-1").truncDecimals());
        assertEquals(ANumber.create("-1"), ANumber.create("-1.5").truncDecimals());
        assertEquals(ANumber.create("-10"), ANumber.create("-10").truncDecimals());
        assertEquals(ANumber.create("-10"), ANumber.create("-10.5").truncDecimals());

        assertEquals(ANumber.create("0"), ANumber.create("0").truncDecimals(0));
        assertEquals(ANumber.create("0"), ANumber.create("0").truncDecimals(1));
        assertEquals(ANumber.create("0"), ANumber.create("0").truncDecimals(0));
        assertEquals(ANumber.create("0.1"), ANumber.create("0.1").truncDecimals(1));
        assertEquals(ANumber.create("0.1"), ANumber.create("0.1").truncDecimals(2));
        assertEquals(ANumber.create("1"), ANumber.create("1").truncDecimals(0));
        assertEquals(ANumber.create("1"), ANumber.create("1").truncDecimals(1));
        assertEquals(ANumber.create("1"), ANumber.create("1.54").truncDecimals(0));
        assertEquals(ANumber.create("1.5"), ANumber.create("1.54").truncDecimals(1));
        assertEquals(ANumber.create("1.54"), ANumber.create("1.54").truncDecimals(2));
        assertEquals(ANumber.create("1.54"), ANumber.create("1.54").truncDecimals(3));
        assertEquals(ANumber.create("10"), ANumber.create("10.54").truncDecimals(0));
        assertEquals(ANumber.create("10.5"), ANumber.create("10.54").truncDecimals(1));
        assertEquals(ANumber.create("10.54"), ANumber.create("10.54").truncDecimals(2));
        assertEquals(ANumber.create("10.54"), ANumber.create("10.54").truncDecimals(3));
        assertEquals(ANumber.create("-0"), ANumber.create("-0").truncDecimals(0));
        assertEquals(ANumber.create("-0"), ANumber.create("-0").truncDecimals(1));
        assertEquals(ANumber.create("-0"), ANumber.create("-0").truncDecimals(0));
        assertEquals(ANumber.create("-0.1"), ANumber.create("-0.1").truncDecimals(1));
        assertEquals(ANumber.create("-0.1"), ANumber.create("-0.1").truncDecimals(2));
        assertEquals(ANumber.create("-1"), ANumber.create("-1").truncDecimals(0));
        assertEquals(ANumber.create("-1"), ANumber.create("-1").truncDecimals(1));
        assertEquals(ANumber.create("-1"), ANumber.create("-1.54").truncDecimals(0));
        assertEquals(ANumber.create("-1.5"), ANumber.create("-1.54").truncDecimals(1));
        assertEquals(ANumber.create("-1.54"), ANumber.create("-1.54").truncDecimals(2));
        assertEquals(ANumber.create("-1.54"), ANumber.create("-1.54").truncDecimals(3));
        assertEquals(ANumber.create("-10"), ANumber.create("-10.54").truncDecimals(0));
        assertEquals(ANumber.create("-10.5"), ANumber.create("-10.54").truncDecimals(1));
        assertEquals(ANumber.create("-10.54"), ANumber.create("-10.54").truncDecimals(2));
        assertEquals(ANumber.create("-10.54"), ANumber.create("-10.54").truncDecimals(3));
    }

    /**
     * Tests equal operator.
     */
    @Test
    public void testEq() {
        assertTrue(ANumber.create(0).eq(ANumber.ZERO));
        assertFalse(ANumber.create(1).eq(ANumber.ZERO));

        // double version
        assertTrue(ANumber.create(0).eq(0));
        assertFalse(ANumber.create(1).eq(0));
    }

    /**
     * Tests not equal operator.
     */
    @Test
    public void testNeq() {
        assertTrue(ANumber.create(1).neq(ANumber.ZERO));
        assertFalse(ANumber.create(0).neq(ANumber.ZERO));

        // double version
        assertTrue(ANumber.create(1).neq(0));
        assertFalse(ANumber.create(0).neq(0));
    }

    /**
     * Tests greater operator.
     */
    @Test
    public void testG() {
        assertTrue(ANumber.create(1).g(ANumber.create(0)));
        assertFalse(ANumber.create(0).g(ANumber.create(0)));
        assertFalse(ANumber.create(-1).g(ANumber.create(0)));

        // double version
        assertTrue(ANumber.create(1).g(0));
        assertFalse(ANumber.create(0).g(0));
        assertFalse(ANumber.create(-1).g(0));
    }

    /**
     * Tests greater or equal operator.
     */
    @Test
    public void testGeq() {
        assertTrue(ANumber.create(1).geq(ANumber.create(0)));
        assertTrue(ANumber.create(0).geq(ANumber.create(0)));
        assertFalse(ANumber.create(-1).geq(ANumber.create(0)));

        // double version
        assertTrue(ANumber.create(1).geq(0));
        assertTrue(ANumber.create(0).geq(0));
        assertFalse(ANumber.create(-1).geq(0));
    }

    /**
     * Tests less operator.
     */
    @Test
    public void testL() {
        assertFalse(ANumber.create(1).l(ANumber.create(0)));
        assertFalse(ANumber.create(0).l(ANumber.create(0)));
        assertTrue(ANumber.create(-1).l(ANumber.create(0)));

        // double version
        assertFalse(ANumber.create(1).l(0));
        assertFalse(ANumber.create(0).l(0));
        assertTrue(ANumber.create(-1).l(0));
    }

    /**
     * Tests less or equal operator.
     */
    @Test
    public void testLeq() {
        assertFalse(ANumber.create(1).leq(ANumber.create(0)));
        assertTrue(ANumber.create(0).leq(ANumber.create(0)));
        assertTrue(ANumber.create(-1).leq(ANumber.create(0)));

        // double version
        assertFalse(ANumber.create(1).leq(0));
        assertTrue(ANumber.create(0).leq(0));
        assertTrue(ANumber.create(-1).leq(0));
    }

    /**
     * Tests conversion to full string.
     */
    @Test
    public void testToFullString() {
        assertEquals("1.5", ANumber.create("1.5").toFullString());
        assertEquals("0.0000000000000000000000001", ANumber.create("1E-25").toFullString());
    }

    /**
     * Tests conversion to the string with fixed decimal points.
     */
    @Test
    public void testToFixedDecimalString() {
        assertEquals("1", ANumber.create("1").toFixedDecimalString(0));
        assertEquals("1", ANumber.create("1.5").toFixedDecimalString(0));
        assertEquals("1.00", ANumber.create("1").toFixedDecimalString(2));
        assertEquals("1.50", ANumber.create("1.5").toFixedDecimalString(2));
        assertEquals("1.50", ANumber.create("1.50").toFixedDecimalString(2));
        assertEquals("1.50", ANumber.create("1.500").toFixedDecimalString(2));
        assertEquals("0.0000000000000000000000001", ANumber.create("1E-25").toFixedDecimalString(25));
        assertEquals("0.00", ANumber.create("1E-25").toFixedDecimalString(2));

        assertEquals("-1", ANumber.create("-1").toFixedDecimalString(0));
        assertEquals("-1", ANumber.create("-1.5").toFixedDecimalString(0));
        assertEquals("-1.00", ANumber.create("-1").toFixedDecimalString(2));
        assertEquals("-1.50", ANumber.create("-1.5").toFixedDecimalString(2));
        assertEquals("-1.50", ANumber.create("-1.50").toFixedDecimalString(2));
        assertEquals("-1.50", ANumber.create("-1.500").toFixedDecimalString(2));
        assertEquals("-0.0000000000000000000000001", ANumber.create("-1E-25").toFixedDecimalString(25));
        assertEquals("-0.00", ANumber.create("-1E-25").toFixedDecimalString(2));
    }

    /**
     * Tests compare to method.
     */
    @Test
    public void testCompareTo() {
        assertEquals(1, ANumber.create(1).compareTo(ANumber.create(0)));
        assertEquals(0, ANumber.create(0).compareTo(ANumber.create(0)));
        assertEquals(-1, ANumber.create(-1).compareTo(ANumber.create(0)));
    }

    /**
     * Tests minimal number.
     */
    @Test
    public void testMin() {
        assertEquals(ANumber.create(1), ANumber.min(ANumber.ONE));
        assertEquals(ANumber.create(1), ANumber.min(ANumber.ONE, ANumber.create(2), ANumber.create(3)));
        assertEquals(ANumber.create(1), ANumber.min(ANumber.create(2), ANumber.create(3), ANumber.create(1)));
    }

    /**
     * Tests maximal number.
     */
    @Test
    public void testMax() {
        assertEquals(ANumber.create(3), ANumber.max(ANumber.create(3)));
        assertEquals(ANumber.create(3), ANumber.max(ANumber.create(3), ANumber.create(2), ANumber.create(1)));
        assertEquals(ANumber.create(3), ANumber.max(ANumber.create(2), ANumber.create(3), ANumber.create(1)));
    }

}
