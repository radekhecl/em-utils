package com.enterprisemath.utils;

/**
 * Id generator definition. Responsibility of this interface is to generate unique identifications used for example
 * as a primary keys or identification codes. It is responsibility of the implementation to guarantee that ids are unique within the required scope.
 *
 * @author radek.hecl
 */
public interface IdGenerator {

    /**
     * Generates new id.
     *
     * @return generated id
     */
    public String generateId();

}
